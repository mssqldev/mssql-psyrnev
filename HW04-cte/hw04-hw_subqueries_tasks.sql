/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "03 - Подзапросы, CTE, временные таблицы".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- Для всех заданий, где возможно, сделайте два варианта запросов:
--  1) через вложенный запрос
--  2) через WITH (для производных таблиц)
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
1. Выберите сотрудников (Application.People), которые являются продажниками (IsSalesPerson), 
и не сделали ни одной продажи 04 июля 2015 года. 
Вывести ИД сотрудника и его полное имя. 
Продажи смотреть в таблице Sales.Invoices.
*/

DECLARE @invoceDate DATETIME = '2015-07-04'

-- NOT IN
SELECT p.PersonID, p.FullName
FROM [Application].[People] as p
WHERE 
	p.IsSalesperson = 1 
	AND p.PersonID NOT IN(
		SELECT i.[SalespersonPersonID]
		FROM [Sales].[Invoices] as i
		WHERE i.[InvoiceDate] = @invoceDate
	)
ORDER BY 1;

-- EXISTS
SELECT p.PersonID, p.FullName
FROM [Application].[People] as p
WHERE p.IsSalesperson = 1 
	AND NOT EXISTS (
		SELECT *
		FROM [Sales].[Invoices] as i
		WHERE i.InvoiceDate = @invoceDate AND i.SalespersonPersonID = p.PersonID
	)
ORDER BY 1;

WITH salePersonByDateCte (SalespersonPersonID) AS (
	SELECT i.[SalespersonPersonID]
	FROM [Sales].[Invoices] as i
	WHERE i.[InvoiceDate] = @invoceDate
)
SELECT p.PersonID, p.FullName
FROM [Application].[People] as p
WHERE 
	p.IsSalesperson = 1 
	AND p.PersonID NOT IN (SELECT * FROM salePersonByDateCte)
ORDER BY 1

/*
SELECT DISTINCT i.SalespersonPersonID
FROM [Sales].[Invoices] as i
WHERE i.InvoiceDate = @invoceDate
ORDER BY 1
*/


/*
2. Выберите товары с минимальной ценой (подзапросом). Сделайте два варианта подзапроса. 
Вывести: ИД товара, наименование товара, цена.
*/

-- BY MIN()
SELECT
	wsi.[StockItemID]
	, wsi.[StockItemName]
	, wsi.[UnitPrice]
FROM [Warehouse].[StockItems] as wsi
WHERE wsi.[UnitPrice] = (SELECT MIN([UnitPrice]) FROM [Warehouse].[StockItems])

-- BY TOP 1 AND ORDER BY
SELECT
	wsi.[StockItemID]
	, wsi.[StockItemName]
	, wsi.[UnitPrice]
FROM [Warehouse].[StockItems] as wsi
WHERE wsi.[UnitPrice] = (SELECT TOP 1 [UnitPrice] FROM [Warehouse].[StockItems] ORDER BY 1)


/*
3. Выберите информацию по клиентам, которые перевели компании пять максимальных платежей 
из Sales.CustomerTransactions. 
Представьте несколько способов (в том числе с CTE). 
*/

/*
SELECT TOP 5 WITH TIES
	sct.[CustomerID]
	, sct.[TransactionAmount]
FROM [Sales].[CustomerTransactions] AS sct
ORDER BY sct.[TransactionAmount] DESC
*/

SELECT *
FROM [Sales].[Customers] AS sc
WHERE sc.[CustomerID] IN (
	SELECT TOP 5 WITH TIES sct.[CustomerID]
	FROM [Sales].[CustomerTransactions] AS sct
	ORDER BY sct.[TransactionAmount] DESC
)

; WITH topTransactionAmountCte AS (
	SELECT TOP 5 WITH TIES 
		sct.[CustomerID]
		, sct.[TransactionAmount]
	FROM [Sales].[CustomerTransactions] AS sct
	ORDER BY sct.[TransactionAmount] DESC
)
SELECT
	cte.TransactionAmount
	, sc.*
FROM [Sales].[Customers] AS sc
	JOIN topTransactionAmountCte as cte ON sc.[CustomerID] = cte.[CustomerID]

/*
4. Выберите города (ид и название), в которые были доставлены товары, 
входящие в тройку самых дорогих товаров, а также имя сотрудника, 
который осуществлял упаковку заказов (PackedByPersonID).
*/
/*
SELECT TOP 3 WITH TIES wsi.[StockItemID]
FROM [Warehouse].[StockItems] AS wsi
ORDER BY wsi.[UnitPrice] DESC
*/
SET STATISTICS IO, TIME ON

-- Op. 1
-- Exe: CPU time = 31 ms,  elapsed time = 104 ms.
; WITH cityVsStockItemCte AS (
	SELECT sc.[DeliveryCityID], sol.[StockItemID], ap.[FullName] AS [PackedBy]
	FROM [Sales].[Customers] AS sc 
		JOIN [Sales].[Orders] AS so ON sc.[CustomerID] = so.[CustomerID]
		JOIN [Sales].[OrderLines] AS sol ON so.[OrderID] = sol.[OrderID]
		JOIN [Sales].[Invoices] AS si ON so.[OrderID] = si.[OrderID]
		JOIN [Application].[People] AS ap ON si.[PackedByPersonID] = ap.[PersonID]
	)
, top3StockItemsCte AS (
	SELECT TOP 3 WITH TIES wsi.[StockItemID]
	FROM [Warehouse].[StockItems] AS wsi
	ORDER BY wsi.[UnitPrice] DESC
)
SELECT DISTINCT 
	ac.[CityID]
	, ac.[CityName]
	, csCte.PackedBy
FROM [Application].[Cities] AS ac
	JOIN cityVsStockItemCte AS csCte ON ac.[CityID] = csCte.[DeliveryCityID]
	JOIN top3StockItemsCte AS tsCte ON csCte.[StockItemID] = tsCte.[StockItemID]
ORDER BY 1

-- op 2.
-- Exe: CPU time = 63 ms,  elapsed time = 88 ms.
; WITH top3StockItemsCte AS (
	SELECT TOP 3 WITH TIES wsi.[StockItemID]
	FROM [Warehouse].[StockItems] AS wsi
	ORDER BY wsi.[UnitPrice] DESC
)
SELECT DISTINCT
	ac.[CityID]
	, ac.[CityName]
	, ap.[FullName] AS PackedBy
FROM top3StockItemsCte AS csCte
	JOIN [Sales].[OrderLines] AS sol ON csCte.[StockItemID] = sol.[StockItemID]
	JOIN [Sales].[Orders] AS so ON sol.[OrderID]= so.[OrderID]
	JOIN [Sales].[Customers] AS sc ON so.[CustomerID] = sc.[CustomerID]
	JOIN [Application].Cities AS ac ON sc.[DeliveryCityID] = ac.[CityID]
	JOIN [Sales].[Invoices] AS si ON so.[OrderID] = si.[OrderID]
	JOIN [Application].People AS ap ON si.[PackedByPersonID] = ap.[PersonID]
ORDER BY 1

-- op. 3
-- CPU time = 47 ms,  elapsed time = 66 ms.
SELECT DISTINCT 
	ac.[CityID]
	, ac.[CityName]
	, ap.[FullName] as PackedBy
FROM [Sales].[OrderLines] as sol
	JOIN [Sales].[Orders] AS so ON sol.[OrderID]= so.[OrderID]
	JOIN [Sales].[Customers] AS sc ON so.[CustomerID] = sc.[CustomerID]
	JOIN [Application].Cities AS ac ON sc.[DeliveryCityID] = ac.[CityID]
	JOIN [Sales].[Invoices] AS si ON so.[OrderID] = si.[OrderID]
	JOIN [Application].People AS ap ON si.[PackedByPersonID] = ap.[PersonID]
WHERE sol.[StockItemID] IN (
		SELECT TOP 3 WITH TIES wsi.[StockItemID]
		FROM [Warehouse].[StockItems] AS wsi
		ORDER BY wsi.[UnitPrice] DESC
	)
ORDER BY 1

-- ---------------------------------------------------------------------------
-- Опциональное задание
-- ---------------------------------------------------------------------------
-- Можно двигаться как в сторону улучшения читабельности запроса, 
-- так и в сторону упрощения плана\ускорения. 
-- Сравнить производительность запросов можно через SET STATISTICS IO, TIME ON. 
-- Если знакомы с планами запросов, то используйте их (тогда к решению также приложите планы). 
-- Напишите ваши рассуждения по поводу оптимизации. 

-- 5. Объясните, что делает и оптимизируйте запрос
SET STATISTICS IO, TIME ON

/*Origin  SQL Server Execution Times:
	CPU time = 219 ms,  elapsed time = 183 ms.
	CPU time = 173 ms,  elapsed time = 207 ms.
	CPU time = 125 ms,  elapsed time = 179 ms.
	CPU time = 110 ms,  elapsed time = 41 ms.
	CPU time = 93 ms,  elapsed time = 43 ms.
	CPU time = 79 ms,  elapsed time = 46 ms.
	CPU time = 64 ms,  elapsed time = 44 ms.
*/
SELECT 
	Invoices.InvoiceID, 
	Invoices.InvoiceDate,
	(SELECT People.FullName
		FROM Application.People
		WHERE People.PersonID = Invoices.SalespersonPersonID
	) AS SalesPersonName,
	SalesTotals.TotalSumm AS TotalSummByInvoice, 
	(SELECT SUM(OrderLines.PickedQuantity*OrderLines.UnitPrice)
		FROM Sales.OrderLines
		WHERE OrderLines.OrderId = (SELECT Orders.OrderId 
			FROM Sales.Orders
			WHERE Orders.PickingCompletedWhen IS NOT NULL	
				AND Orders.OrderId = Invoices.OrderId)	
	) AS TotalSummForPickedItems
FROM Sales.Invoices 
	JOIN
	(SELECT InvoiceId, SUM(Quantity*UnitPrice) AS TotalSumm
	FROM Sales.InvoiceLines
	GROUP BY InvoiceId
	HAVING SUM(Quantity*UnitPrice) > 27000) AS SalesTotals
		ON Invoices.InvoiceID = SalesTotals.InvoiceID
ORDER BY TotalSumm DESC
-- --
/*
выбираем счета с суммой более 27000
выводим: 
	- выставленную сумму (TotalSummByInvoice)
	- фактическую сумму за принятый товар (TotalSummForPickedItems)
*/

/* 
C точки зрения читаемости можно:
 - вынести подзапросы в common table expression (добавим суффикс cte для прозрачности)
 - вычисление SalesPersonName перенесем в JOIN 
*/
-- SalesTotalsCte - останется без изменений
; WITH SalesTotalsCte AS (
	SELECT [InvoiceId], SUM([Quantity] * [UnitPrice]) AS [TotalSummByInvoice]
	FROM [Sales].[InvoiceLines]
	GROUP BY [InvoiceID]
	HAVING SUM([Quantity] * UnitPrice) > 27000
	)
-- TotalSummForPickedItems - изменим запрос, так чтоб получить таблицу вида [OrderID], [TotalSummForPickedItems]
, TotalPickedOrderCte AS (
	SELECT
		sol.[OrderID]
		, SUM(sol.[PickedQuantity] * sol.[UnitPrice]) AS TotalSummForPickedItems
	FROM [Sales].[OrderLines] AS sol
		JOIN [Sales].[Orders] AS so ON sol.[OrderID] = so.[OrderID]
	WHERE so.PickingCompletedWhen IS NOT NULL
	GROUP BY sol.[OrderID]
)
SELECT 
	si.[InvoiceID]
	, si.[InvoiceDate]
	, ap.[FullName] AS SalesPersonName
	, stCte.[TotalSummByInvoice]
	, tpoCte.[TotalSummForPickedItems]
FROM Sales.Invoices AS si
-- соберем всё вместе
	JOIN SalesTotalsCte AS stCte ON si.[InvoiceID] = stCte.[InvoiceID]
	JOIN TotalPickedOrderCte AS tpoCte ON si.[OrderID] = tpoCte.[OrderID]
	JOIN [Application].[People] as ap ON si.[SalespersonPersonID] = ap.[PersonID]
ORDER BY [TotalSummByInvoice] DESC

/*
Refactored SQL Server Execution Times:
	CPU time = 79 ms,  elapsed time = 190 ms.
	CPU time = 78 ms,  elapsed time = 194 ms.
	CPU time = 62 ms,  elapsed time = 195 ms.
	CPU time = 63 ms,  elapsed time = 62 ms.
	CPU time = 62 ms,  elapsed time = 61 ms.
	CPU time = 47 ms,  elapsed time = 63 ms.
*/