/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "05 - Операторы CROSS APPLY, PIVOT, UNPIVOT".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
1. Требуется написать запрос, который в результате своего выполнения 
формирует сводку по количеству покупок в разрезе клиентов и месяцев.
В строках должны быть месяцы (дата начала месяца), в столбцах - клиенты.

Клиентов взять с ID 2-6, это все подразделение Tailspin Toys.
Имя клиента нужно поменять так чтобы осталось только уточнение.
Например, исходное значение "Tailspin Toys (Gasport, NY)" - вы выводите только "Gasport, NY".
Дата должна иметь формат dd.mm.yyyy, например, 25.12.2019.

Пример, как должны выглядеть результаты:
-------------+--------------------+--------------------+-------------+--------------+------------
InvoiceMonth | Peeples Valley, AZ | Medicine Lodge, KS | Gasport, NY | Sylvanite, MT | Jessie, ND
-------------+--------------------+--------------------+-------------+--------------+------------
01.01.2013   |      3             |        1           |      4      |      2        |     2
01.02.2013   |      7             |        3           |      4      |      2        |     1
-------------+--------------------+--------------------+-------------+--------------+------------
*/

-- Список колонок для IN 
DECLARE @ListToPivot NVARCHAR(200)
SELECT
	@ListToPivot = STRING_AGG(
		QUOTENAME(
			SUBSTRING(
				sc.[CustomerName], 
				PATINDEX('%(%)', sc.[CustomerName]) + 1,
				PATINDEX('%)', sc.[CustomerName]) - PATINDEX('%(%)', sc.[CustomerName]) - 1
			)
		)
		, ', '
	)
FROM [Sales].[Customers] AS sc 
WHERE sc.[CustomerID] BETWEEN 2 AND 6;
SELECT @ListToPivot;

SELECT *
FROM (
	SELECT
		SUBSTRING(
			sc.[CustomerName], 
			PATINDEX('%(%)', sc.[CustomerName]) + 1,
			PATINDEX('%)', sc.[CustomerName]) - PATINDEX('%(%)', sc.[CustomerName]) - 1
			) AS [NamePart]
		, DATETRUNC(MONTH, si.[InvoiceDate]) AS [InvoiceMonth]
		, si.[InvoiceID]
	FROM [Sales].[Customers] AS sc
		JOIN [Sales].[Invoices] si ON sc.[CustomerID] = si.[CustomerID]
	WHERE sc.[CustomerID] BETWEEN 2 AND 6
	) AS subQ
	PIVOT 
	(
		COUNT(subQ.[InvoiceID])
		FOR subQ.[NamePart]
		IN ([Sylvanite, MT], [Peeples Valley, AZ], [Medicine Lodge, KS], [Gasport, NY], [Jessie, ND])
	) AS p

/*
2. Для всех клиентов с именем, в котором есть "Tailspin Toys"
вывести все адреса, которые есть в таблице, в одной колонке.

Пример результата:
----------------------------+--------------------
CustomerName                | AddressLine
----------------------------+--------------------
Tailspin Toys (Head Office) | Shop 38
Tailspin Toys (Head Office) | 1877 Mittal Road
Tailspin Toys (Head Office) | PO Box 8975
Tailspin Toys (Head Office) | Ribeiroville
----------------------------+--------------------
*/

SELECT 
	unpSubQ.[CustomerName]
	, unpSubQ.[AddressLine]
FROM (
	SELECT
		sc.[CustomerName]
		, sc.[DeliveryAddressLine1]
		, sc.[DeliveryAddressLine2]
		, sc.[PostalAddressLine1]
		, sc.[PostalAddressLine2]
	FROM [Sales].[Customers] AS sc 
	WHERE sc.[CustomerName] LIKE 'Tailspin Toys%'
	) AS subQ
	UNPIVOT
	(
		[AddressLine] FOR _adddresSrcColumn IN([DeliveryAddressLine1], [DeliveryAddressLine2], [PostalAddressLine1], [PostalAddressLine2])
	) AS unpSubQ

/*
3. В таблице стран (Application.Countries) есть поля с цифровым кодом страны и с буквенным.
Сделайте выборку ИД страны, названия и ее кода так, 
чтобы в поле с кодом был либо цифровой либо буквенный код.

Пример результата:
--------------------------------
CountryId | CountryName | Code
----------+-------------+-------
1         | Afghanistan | AFG
1         | Afghanistan | 4
3         | Albania     | ALB
3         | Albania     | 8
----------+-------------+-------
*/
SELECT
	unpSubQ.[CountryID]
	, unpSubQ.[CountryName]
	, unpSubQ.[Code]
FROM (
	SELECT
		ac.[CountryID]
		, ac.[CountryName]
		, ac.[IsoAlpha3Code]
		, CAST(ac.[IsoNumericCode] AS nvarchar(3)) AS [IsoNumericCode]
	FROM [Application].[Countries] AS ac
	) AS subQ
	UNPIVOT
	(
		Code FOR _codeColumnSource IN ([IsoAlpha3Code], [IsoNumericCode])
	) AS unpSubQ

/*
4. Выберите по каждому клиенту два самых дорогих товара, которые он покупал.
В результатах должно быть ид клиета, его название, ид товара, цена, дата покупки.
*/

SELECT
	sc.[CustomerID]
	, sc.[CustomerName]
	, subQ.[StockItemID]
	, subQ.[UnitPrice]
	, subQ.[InvoiceDate]
FROM [Sales].[Customers] AS sc
	CROSS APPLY (
		SELECT TOP 2 WITH TIES
			 silSubQ.[StockItemID]
			, silSubQ.[UnitPrice]
			, MIN(si.[InvoiceDate]) AS [InvoiceDate]
        FROM [Sales].[Invoices] AS si
			CROSS APPLY (
				SELECT
					sil.[StockItemID]
					, sil.[UnitPrice]
				FROM [Sales].[InvoiceLines] AS sil
				WHERE sil.[InvoiceID] = si.[InvoiceID]
			) AS silSubQ
        WHERE si.[CustomerID] = sc.[CustomerID]
		GROUP BY silSubQ.[StockItemID], silSubQ.[UnitPrice]
        ORDER BY silSubQ.[UnitPrice] DESC) AS subQ