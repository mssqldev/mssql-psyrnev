
### Links:
- [Руководство по архитектуре и разработке индексов SQL Server и Azure SQL](https://learn.microsoft.com/ru-ru/sql/relational-databases/sql-server-index-design-guide?view=sql-server-ver15)
- [Компонент Full-text Search](https://learn.microsoft.com/ru-ru/sql/relational-databases/search/full-text-search?view=sql-server-ver15)
- [XML-индексы (SQL Server)](https://learn.microsoft.com/ru-ru/sql/relational-databases/xml/xml-indexes-sql-server?view=sql-server-2017)
- [YT: Колоночные индексы в SQL Server 2016](https://www.youtube.com/watch?v=QGK-EvYKarY)