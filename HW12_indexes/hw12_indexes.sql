﻿USE DreamTeam;

-- Расписание на матчей на следующую неделю
-- 1. Все матчи. Вида: Дата Команда Хозяев, Команда Гостей

SELECT
	e.[Date]
	, homeClub.[Title] AS [Home]
	, guestClub.[Title] AS [Guest]
FROM dt.Events AS e
	INNER JOIN dt.EventApplication AS homeEa ON e.[HomeClubApplicationID] = homeEa.[ApplicationID] 
	INNER JOIN dt.EventApplication AS guestEa ON e.[GuestClubApplicationID] = guestEa.[ApplicationID]
	INNER JOIN dt.Club AS homeClub ON homeEa.[ClubID] = homeClub.[ClubID]
	INNER JOIN dt.Club AS guestClub ON guestEa.[ClubID] = guestClub.[ClubID]
WHERE e.[Date] BETWEEN '2024-09-01' AND '2024-10-01'

SELECT e.[Date], e.[HomeClubApplicationID], e.[GuestClubApplicationID]
FROM dt.Events AS e
WHERE e.[Date] >= '2024-09-01' AND e.[Date] < '2024-10-01'

DROP INDEX IF EXISTS [dt_Events_Date] ON dt.[Events]
CREATE NONCLUSTERED INDEX [dt_Events_Date]
ON dt.[Events]
(
	[Date] ASC
)
INCLUDE([HomeClubApplicationID], [GuestClubApplicationID])
GO

-- 2. Для выбранного клуба
SELECT
	e.[Date]
	, homeClub.[Title] AS [Home]
	, guestClub.[Title] AS [Guest]
FROM dt.Events AS e
	INNER JOIN dt.EventApplication AS homeEa ON e.[HomeClubApplicationID] = homeEa.[ApplicationID] 
	INNER JOIN dt.EventApplication AS guestEa ON e.[GuestClubApplicationID] = guestEa.[ApplicationID]
	INNER JOIN dt.[Club] AS homeClub ON homeEa.[ClubID] = homeClub.[ClubID]
	INNER JOIN dt.[Club] AS guestClub ON guestEa.[ClubID] = guestClub.[ClubID]
WHERE homeClub.[Title] = N'Snowflakes' OR guestClub.[Title] = N'Snowflakes'
ORDER BY e.[Date] ASC

EXEC dt.GetScheduleByClub @clubTitle=N'Snowflakes'

DROP INDEX IF EXISTS [dt_Club_Title] ON dt.[Club]
CREATE NONCLUSTERED INDEX [dt_Club_Title]
ON dt.[Club]
(
	[Title] ASC
)
GO

-- 3. Для выбранного клуба только домашние

EXEC dt.GetHomeScheduleByClub @clubTitle=N'Snowflakes'

-- Агрегированная статистика по матчам
--1. Для выбранного клуба

SELECT
	c.[Title]
	, m.[Name]
	, COUNT(*)
FROM dt.[Club] AS c
	INNER JOIN dt.[EventApplication] AS ea ON c.[ClubID] = ea.[ClubID]
	INNER JOIN dt.[Events] AS e ON ea.[ApplicationID] = e.[HomeClubApplicationID]
		OR ea.[ApplicationID] = e.[GuestClubApplicationID]
	INNER JOIN dt.[EventLog] AS el ON e.[EventID] = el.[EventID] 
	INNER JOIN dt.[Measurement] AS m ON el.[MeasurementID] = m.[MeasurementID] 
WHERE e.[Date] < '2024-09-27'
	AND c.[Title] = N'Snowflakes'
	AND el.[PlayerID] IN (
			SELECT bcp.[PlayerID]
			FROM dt.[b_Club_Player] AS bcp
			WHERE bcp.[ClubID] = c.[ClubID]
		)
GROUP BY c.[Title], m.[Name]

SELECT COUNT(1)
FROM [dt].[Events] e
	INNER JOIN dt.[EventApplication] ea ON e.GuestClubApplicationID = ea.ApplicationID 
		OR e.HomeClubApplicationID = ea.ApplicationID
	INNER JOIN dt.EventLog el ON e.EventID = el.EventID
WHERE e.[Date] < '2024-09-27' AND ea.ClubID = 'DD616C82-9373-EF11-8CA6-E02BE9B0A1D1'

EXEC dt.GetClubStats @clubTitle=N'Snowflakes', @uptoDate='2024-09-27'
-- Агрегированная статистика игрока
-- 1. за выбранный период

EXEC dt.GetPlayerStatsForPeriod @PlayerSecondName=N'Ovechkin', @from = '2024-09-01', @to = '2024-09-27'

SELECT STRING_AGG([Name], '", "')
FROM dt.[Measurement]

EXEC dt.GetPlayerStatsForPeriodPivoted @PlayerSecondName=N'Ovechkin', @from = '2024-09-01', @to = '2024-09-27'