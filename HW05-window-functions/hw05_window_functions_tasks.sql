/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "06 - Оконные функции".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters
/*
1. Сделать расчет суммы продаж нарастающим итогом по месяцам с 2015 года 
(в рамках одного месяца он будет одинаковый, нарастать будет в течение времени выборки).
Выведите: id продажи, название клиента, дату продажи, сумму продажи, сумму нарастающим итогом

Пример:
-------------+----------------------------
Дата продажи | Нарастающий итог по месяцу
-------------+----------------------------
 2015-01-29   | 4801725.31
 2015-01-30	 | 4801725.31
 2015-01-31	 | 4801725.31
 2015-02-01	 | 9626342.98
 2015-02-02	 | 9626342.98
 2015-02-03	 | 9626342.98
Продажи можно взять из таблицы Invoices.
Нарастающий итог должен быть без оконной функции.
*/

WITH salesCte AS (
	SELECT
		si.[InvoiceID]
		, si.[CustomerID]
		, si.[InvoiceDate]
		, SUM(sil.[UnitPrice] * sil.[Quantity]) AS [InvoiceCost]
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	WHERE si.[InvoiceDate] >= '2015-01-01 00:00:00'
	GROUP BY si.[InvoiceID], si.[CustomerID], si.[InvoiceDate]
)
, mothlyCostsCte AS (
	SELECT
		DATETRUNC(MONTH, si.[InvoiceDate]) AS [InvoiceMonth]
		, SUM(sil.[UnitPrice] * sil.[Quantity]) AS [Cost]
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	WHERE si.[InvoiceDate] >= '2015-01-01 00:00:00'
	GROUP BY DATETRUNC(MONTH, si.[InvoiceDate])
)
SELECT
	salesCte.[InvoiceID]
	, salesCte.[InvoiceDate]
	, sc.[CustomerName]
	, salesCte.[InvoiceCost]
	, (
		SELECT SUM(mcCte.[Cost])
		FROM mothlyCostsCte AS mcCte
		WHERE salesCte.[InvoiceDate] >= mcCte.[InvoiceMonth] 
	) AS CumulativeTotal
FROM salesCte JOIN [Sales].[Customers] AS sc ON salesCte.[CustomerID] = sc.[CustomerID]
ORDER BY salesCte.[InvoiceDate]

/*
2. Сделайте расчет суммы нарастающим итогом в предыдущем запросе с помощью оконной функции.
   Сравните производительность запросов 1 и 2 с помощью set statistics time, io on
*/
WITH salesCte AS (
	SELECT
		si.[InvoiceID]
		, si.[CustomerID]
		, si.[InvoiceDate]
		, SUM(sil.[UnitPrice] * sil.[Quantity]) AS TotalCost
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	WHERE si.[InvoiceDate] >= '2015-01-01 00:00:00'
	GROUP BY si.[InvoiceID], si.[CustomerID], si.[InvoiceDate]
)
SELECT
	salesCte.[InvoiceID]
	, salesCte.[InvoiceDate]
	, sc.[CustomerName]
	, salesCte.[TotalCost]
	, SUM(salesCte.[TotalCost]) OVER (ORDER BY DATETRUNC(MONTH, salesCte.[InvoiceDate])) AS CumulativeTotal
FROM salesCte JOIN [Sales].[Customers] AS sc ON salesCte.[CustomerID] = sc.[CustomerID]
ORDER BY salesCte.[InvoiceDate]

/*Помесячный нарастающий итог*/ 
SELECT
	subQ.[InvoiceMonth]
	, subQ.[Cost]
	, SUM(subQ.[Cost]) OVER (ORDER BY subQ.[InvoiceMonth]) AS CumulativeTotal
FROM (
	SELECT
		DATETRUNC(MONTH, si.[InvoiceDate]) AS [InvoiceMonth]
		, SUM(sil.[UnitPrice] * sil.[Quantity]) AS [Cost]
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	WHERE si.[InvoiceDate] >= '2015-01-01 00:00:00'
	GROUP BY DATETRUNC(MONTH, si.[InvoiceDate])
	) AS subQ
ORDER BY 1

/*
3. Вывести список 2х самых популярных продуктов (по количеству проданных) 
в каждом месяце за 2016 год (по 2 самых популярных продукта в каждом месяце).
*/

WITH quatityCte AS (
	SELECT
		sil.[StockItemID]
		, DATETRUNC(MONTH, si.[InvoiceDate]) AS InvoiceMonth
		, SUM(sil.[Quantity]) AS [Quantity]
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	WHERE si.[InvoiceDate] >= '2016-01-01 00:00:00'
	GROUP BY sil.[StockItemID], DATETRUNC(MONTH, si.[InvoiceDate])
)
SELECT
	wsi.[StockItemName]
	, subQ.[InvoiceMonth]
	, subQ.[Quantity]
FROM (
	SELECT 
		qCte.*
		, DENSE_RANK() OVER (PARTITION BY qCte.[InvoiceMonth] ORDER BY qCte.[Quantity] DESC) AS [RNK]
	FROM quatityCte AS qCte
	) AS subQ
	JOIN [Warehouse].[StockItems] AS wsi ON subQ.[StockItemID] = wsi.[StockItemID]
WHERE subQ.RNK <= 2
ORDER BY 2, 3 DESC

/*
4. Функции одним запросом
Посчитайте по таблице товаров (в вывод также должен попасть ид товара, название, брэнд и цена):
* пронумеруйте записи по названию товара, так чтобы при изменении буквы алфавита нумерация начиналась заново
* посчитайте общее количество товаров и выведете полем в этом же запросе
* посчитайте общее количество товаров в зависимости от первой буквы названия товара
* отобразите следующий id товара исходя из того, что порядок отображения товаров по имени 
* предыдущий ид товара с тем же порядком отображения (по имени)
* названия товара 2 строки назад, в случае если предыдущей строки нет нужно вывести "No items"
* сформируйте 30 групп товаров по полю вес товара на 1 шт

Для этой задачи НЕ нужно писать аналог без аналитических функций.
*/

SELECT
	wsi.[StockItemID]
	, wsi.[StockItemName]
	, wsi.[Brand]
	, wsi.[UnitPrice]
	-- пронумеруйте записи по названию товара, так чтобы при изменении буквы алфавита нумерация начиналась заново
	, ROW_NUMBER() OVER (PARTITION BY  LEFT(wsi.[StockItemName],1) ORDER BY wsi.[StockItemName]) AS [NumOverFirstLetter]
	-- посчитайте общее количество товаров и выведете полем в этом же запросе
	, COUNT(*) OVER () AS [TotalCount]
	-- посчитайте общее количество товаров в зависимости от первой буквы названия товара
	, COUNT(*) OVER (PARTITION BY  LEFT(wsi.[StockItemName],1)) AS [CountOverFirstLetter]
	-- отобразите следующий id товара исходя из того, что порядок отображения товаров по имени
	, LEAD(wsi.[StockItemID]) OVER (ORDER BY wsi.[StockItemName]) AS [NextId]
	-- предыдущий ид товара с тем же порядком отображения (по имени)
	, LAG(wsi.[StockItemID]) OVER (ORDER BY wsi.[StockItemName]) AS [PreviousId]
	-- названия товара 2 строки назад, в случае если предыдущей строки нет нужно вывести "No items"
	, LAG(wsi.[StockItemName], 2, N'No items') OVER (ORDER BY wsi.[StockItemName]) AS [PreviousId]
	-- сформируйте 30 групп товаров по полю вес товара на 1 шт
	, NTILE(30) OVER (ORDER BY wsi.[TypicalWeightPerUnit]) AS [GroupByWeight]
FROM [Warehouse].[StockItems] AS wsi

/*
5. По каждому сотруднику выведите последнего клиента, которому сотрудник что-то продал.
   В результатах должны быть 
   ид и фамилия сотрудника,
   ид и название клиента,
   дата продажи,
   сумму сделки.
*/

SELECT subQ.[PersonID]
	, subQ.[PersonName]
	, subQ.[CustomerID]
	, subQ.[CustomerName]
	, subQ.[InvoiceDate]
	, subQ.[InvoiceCost]
FROM (
	SELECT
		ap.[PersonID]
		, ap.[FullName] AS [PersonName]
		, sc.[CustomerID], sc.[CustomerName]
		, si.[InvoiceDate]
		, SUM(sil.[UnitPrice] * sil.[Quantity]) OVER (PARTITION BY sil.[InvoiceID]) AS [InvoiceCost]
		, ROW_NUMBER() OVER (PARTITION BY ap.[PersonID] ORDER BY si.[InvoiceDate] DESC) AS [RN]
	FROM [Application].[People] AS ap
		JOIN [Sales].[Invoices] AS si ON ap.[PersonID] = si.[SalespersonPersonID]
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
		JOIN [Sales].[Customers] AS sc ON si.[CustomerID] = sc.[CustomerID]
	WHERE ap.[IsSalesperson] = 1
	) AS subQ
WHERE subQ.[RN] = 1


/*
6. Выберите по каждому клиенту два самых дорогих товара, которые он покупал.
В результатах должно быть ид клиета, его название, ид товара, цена, дата покупки.
*/

SELECT
	subQ.[CustomerID]
	, subQ.[CustomerName]
	, subQ.[InvoiceDate]
	, subQ.[StockItemID]
	, subQ.[UnitPrice]
FROM (
	SELECT
		sc.[CustomerID]
		, sc.[CustomerName]
		, si.[InvoiceDate]
		, sil.[StockItemID]
		, sil.[UnitPrice]
		-- ранжируем внутри окна [CustomerID] упорядоченому по [UnitPrice]
		, DENSE_RANK() OVER (PARTITION BY sc.[CustomerID] ORDER BY sil.[UnitPrice] DESC) AS [RN]
		-- нумеруем строки внитри окна [CustomerID] [StockItemID] упорядоченому по [UnitPrice]
		--, ROW_NUMBER() OVER (PARTITION BY sc.[CustomerID], sil.[StockItemID] ORDER BY sil.[UnitPrice] DESC) AS [R]

	FROM [Sales].[Customers] AS sc
		JOIN [Sales].[Invoices] AS si ON sc.[CustomerID] = si.[CustomerID]
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	) AS subQ
WHERE 
	-- берем только 1 и 2 ранги по цене
	subQ.[RN] <= 2
	-- берем только первую строку
	-- AND subQ.[R] = 1
	--AND subQ.CustomerID = 106
ORDER BY 2, [UnitPrice] DESC


--Опционально можете для каждого запроса без оконных функций сделать вариант запросов с оконными функциями и сравнить их производительность. 