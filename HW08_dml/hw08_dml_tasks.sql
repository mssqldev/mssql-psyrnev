/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "10 - Операторы изменения данных".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
1. Довставлять в базу пять записей используя insert в таблицу Customers или Suppliers 
*/

SELECT *
FROM [Sales].[Customers]

SELECT *
FROM [Sales].[CustomerCategories]

SELECT ap.PersonID
FROM [Application].[People] AS ap
WHERE ap.IsSystemUser = 0


SELECT STRING_AGG(name, ', ')
FROM sys.all_columns
WHERE object_id = OBJECT_ID('WideWorldImporters.Sales.Customers') AND is_nullable = 0 AND generated_always_type = 0

SELECT *
FROM sys.all_columns
WHERE object_id = OBJECT_ID('WideWorldImporters.Sales.Customers') AND generated_always_type >= 1

SELECT CONVERT(int, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId'

DROP PROCEDURE IF EXISTS Sales.GetRandomCustomerCategoryID
GO
CREATE PROC Sales.GetRandomCustomerCategoryID AS
BEGIN
	SELECT TOP 1 [CustomerCategoryID]
	FROM [Sales].[CustomerCategories]
	ORDER BY NEWID();
END;

DROP PROCEDURE IF EXISTS Sales.GetCurrentCustomerId
GO
CREATE PROC Sales.GetCurrentCustomerId AS
BEGIN
	SELECT CAST(current_value AS int) FROM sys.sequences WHERE name = 'CustomerId';
END;

DROP PROCEDURE IF EXISTS [Application].GetRandomPrimaryContactPersonID
GO
CREATE PROC [Application].GetRandomPrimaryContactPersonID AS
BEGIN
	SELECT TOP 1 ap.PersonID
	FROM [Application].[People] AS ap
	WHERE ap.IsSystemUser = 0
	ORDER BY NEWID();
END;

DROP PROCEDURE IF EXISTS [Application].GetRandomDeliveryMethodID
GO
CREATE PROC [Application].GetRandomDeliveryMethodID AS
BEGIN
	SELECT TOP 1 adm.DeliveryMethodID
	FROM [Application].[DeliveryMethods] AS adm
	ORDER BY NEWID();
END;


--EXEC Sales.GetRandomCustomerCategoryID
--EXEC Sales.GetCurrentCustomerId
--EXEC [Application].GetRandomPrimaryContactPersonID
--EXEC [Application].GetRandomDeliveryMethodID


INSERT INTO [Sales].[Customers]
	(
		CustomerID
		, CustomerName
		, BillToCustomerID
		, CustomerCategoryID
		, PrimaryContactPersonID
		, DeliveryMethodID
		, DeliveryCityID
		, PostalCityID
		, AccountOpenedDate
		, StandardDiscountPercentage
		, IsStatementSent
		, IsOnCreditHold
		, PaymentDays
		, PhoneNumber
		, FaxNumber
		, WebsiteURL
		, DeliveryAddressLine1
		, DeliveryPostalCode
		, PostalAddressLine1
		, PostalPostalCode
		, LastEditedBy
	)
VALUES (
	/*CustomerID*/ CONVERT(int, NEXT VALUE FOR [Sequences].[CustomerID])
	/*CustomerName*/, 'Customer1' 
	/*BillToCustomerID*/, (SELECT CONVERT(int, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
	/*CustomerCategoryID*/, (SELECT TOP 1 [CustomerCategoryID] FROM [Sales].[CustomerCategories] ORDER BY NEWID())
	/*PrimaryContactPersonID*/, (SELECT TOP 1 ap.PersonID FROM [Application].[People] AS ap WHERE ap.IsSystemUser = 0 ORDER BY NEWID())
	/*DeliveryMethodID*/, (SELECT TOP 1 adm.DeliveryMethodID FROM [Application].[DeliveryMethods] AS adm ORDER BY NEWID())
	/*DeliveryCityID*/, 19586
	/*PostalCityID*/, 19586
	/*AccountOpenedDate*/, GETDATE()
	/*StandardDiscountPercentage*/, 0.01
	/*IsStatementSent*/, 0
	/*IsOnCreditHold*/, 0
	/*PaymentDays*/, 7
	/*PhoneNumber*/, N'(701) 555-2222'
	/*FaxNumber*/, N'(701) 555-3333'
	/*WebsiteURL*/, N'www.c1.org'
	/*DeliveryAddressLine1*/, N'DeliveryAddressLine1'
	/*DeliveryPostalCode*/, N'600039'
	/*PostalAddressLine1*/, N'PostalAddressLine1'
	/*PostalPostalCode*/, N'600000'
	/*LastEditedBy*/, (SELECT CONVERT(INT, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
),
(
	/*CustomerID*/ CONVERT(int, NEXT VALUE FOR [Sequences].[CustomerID])
	/*CustomerName*/, 'Customer2'
	/*BillToCustomerID*/, (SELECT CONVERT(int, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
	/*CustomerCategoryID*/, (SELECT TOP 1 [CustomerCategoryID] FROM [Sales].[CustomerCategories] ORDER BY NEWID())
	/*PrimaryContactPersonID*/, (SELECT TOP 1 ap.PersonID FROM [Application].[People] AS ap WHERE ap.IsSystemUser = 0 ORDER BY NEWID())
	/*DeliveryMethodID*/, (SELECT TOP 1 adm.DeliveryMethodID FROM [Application].[DeliveryMethods] AS adm ORDER BY NEWID())
	/*DeliveryCityID*/, 33475
	/*PostalCityID*/, 33475
	/*AccountOpenedDate*/, GETDATE()-1
	/*StandardDiscountPercentage*/, 0.00
	/*IsStatementSent*/, 0
	/*IsOnCreditHold*/, 0
	/*PaymentDays*/, 7
	/*PhoneNumber*/, N'(701) 555-1111'
	/*FaxNumber*/, N'(701) 555-0000'
	/*WebsiteURL*/, N'www.c2.org'
	/*DeliveryAddressLine1*/, N'DeliveryAddressLine2'
	/*DeliveryPostalCode*/, N'600030'
	/*PostalAddressLine1*/, N'PostalAddressLine2'
	/*PostalPostalCode*/, N'600000'
	/*LastEditedBy*/, (SELECT CONVERT(INT, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
),
(
	/*CustomerID*/ CONVERT(int, NEXT VALUE FOR [Sequences].[CustomerID])
	/*CustomerName*/, 'Customer3'
	/*BillToCustomerID*/, (SELECT CONVERT(int, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
	/*CustomerCategoryID*/, (SELECT TOP 1 [CustomerCategoryID] FROM [Sales].[CustomerCategories] ORDER BY NEWID())
	/*PrimaryContactPersonID*/, (SELECT TOP 1 ap.PersonID FROM [Application].[People] AS ap WHERE ap.IsSystemUser = 0 ORDER BY NEWID())
	/*DeliveryMethodID*/, (SELECT TOP 1 adm.DeliveryMethodID FROM [Application].[DeliveryMethods] AS adm ORDER BY NEWID())
	/*DeliveryCityID*/, 33475
	/*PostalCityID*/, 33475
	/*AccountOpenedDate*/, GETDATE()-1
	/*StandardDiscountPercentage*/, 0.00
	/*IsStatementSent*/, 0
	/*IsOnCreditHold*/, 0
	/*PaymentDays*/, 7
	/*PhoneNumber*/, N'(701) 555-3333'
	/*FaxNumber*/, N'(701) 555-3331'
	/*WebsiteURL*/, N'www.c3.org'
	/*DeliveryAddressLine1*/, N'DeliveryAddressLine3'
	/*DeliveryPostalCode*/, N'600033'
	/*PostalAddressLine1*/, N'PostalAddressLine3'
	/*PostalPostalCode*/, N'600000'
	/*LastEditedBy*/, (SELECT CONVERT(INT, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
),
(
	/*CustomerID*/ CONVERT(int, NEXT VALUE FOR [Sequences].[CustomerID])
	/*CustomerName*/, 'Customer4'
	/*BillToCustomerID*/, (SELECT CONVERT(int, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
	/*CustomerCategoryID*/, (SELECT TOP 1 [CustomerCategoryID] FROM [Sales].[CustomerCategories] ORDER BY NEWID())
	/*PrimaryContactPersonID*/, (SELECT TOP 1 ap.PersonID FROM [Application].[People] AS ap WHERE ap.IsSystemUser = 0 ORDER BY NEWID())
	/*DeliveryMethodID*/, (SELECT TOP 1 adm.DeliveryMethodID FROM [Application].[DeliveryMethods] AS adm ORDER BY NEWID())
	/*DeliveryCityID*/, 33475
	/*PostalCityID*/, 33475
	/*AccountOpenedDate*/, GETDATE()-1
	/*StandardDiscountPercentage*/, 0.00
	/*IsStatementSent*/, 0
	/*IsOnCreditHold*/, 0
	/*PaymentDays*/, 7
	/*PhoneNumber*/, N'(701) 555-4444'
	/*FaxNumber*/, N'(701) 555-4441'
	/*WebsiteURL*/, N'www.c4.org'
	/*DeliveryAddressLine1*/, N'DeliveryAddressLine4'
	/*DeliveryPostalCode*/, N'600044'
	/*PostalAddressLine1*/, N'PostalAddressLine4'
	/*PostalPostalCode*/, N'600000'
	/*LastEditedBy*/, (SELECT CONVERT(INT, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
),
(
	/*CustomerID*/ CONVERT(int, NEXT VALUE FOR [Sequences].[CustomerID])
	/*CustomerName*/, 'Customer5'
	/*BillToCustomerID*/, (SELECT CONVERT(int, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
	/*CustomerCategoryID*/, (SELECT TOP 1 [CustomerCategoryID] FROM [Sales].[CustomerCategories] ORDER BY NEWID())
	/*PrimaryContactPersonID*/, (SELECT TOP 1 ap.PersonID FROM [Application].[People] AS ap WHERE ap.IsSystemUser = 0 ORDER BY NEWID())
	/*DeliveryMethodID*/, (SELECT TOP 1 adm.DeliveryMethodID FROM [Application].[DeliveryMethods] AS adm ORDER BY NEWID())
	/*DeliveryCityID*/, 33475
	/*PostalCityID*/, 33475
	/*AccountOpenedDate*/, GETDATE()-1
	/*StandardDiscountPercentage*/, 0.00
	/*IsStatementSent*/, 0
	/*IsOnCreditHold*/, 0
	/*PaymentDays*/, 7
	/*PhoneNumber*/, N'(701) 555-5555'
	/*FaxNumber*/, N'(701) 555-5551'
	/*WebsiteURL*/, N'www.c5.org'
	/*DeliveryAddressLine1*/, N'DeliveryAddressLine5'
	/*DeliveryPostalCode*/, N'600055'
	/*PostalAddressLine1*/, N'PostalAddressLine5'
	/*PostalPostalCode*/, N'600000'
	/*LastEditedBy*/, (SELECT CONVERT(INT, current_value) AS CurrentValue FROM sys.sequences WHERE name = 'CustomerId')
)

/*
2. Удалите одну запись из Customers, которая была вами добавлена
*/

SELECT *
FROM [Sales].[Customers] as sc
WHERE sc.[CustomerName] = N'Customer3'

DELETE FROM [Sales].[Customers]
WHERE [CustomerName] = N'Customer3'

/*
3. Изменить одну запись, из добавленных через UPDATE
*/

UPDATE [Sales].[Customers]
SET 
	[DeliveryCityID] = 26483
	, [PostalCityID] = 26483
OUTPUT 
    inserted.[DeliveryCityID] as new_deliveryCityID
  , inserted.[PostalCityID] as new_postalCityID
  , deleted.[DeliveryCityID] as old_deliveryCityID
  , deleted.[PostalCityID] old_postalCityID
WHERE [CustomerName] = N'Customer4'

/*
4. Написать MERGE, который вставит запись в клиенты, если ее там нет, и изменит если она уже есть
*/

-- Подготовим таблицу с исходными данными
--DROP TABLE IF EXISTS [Sales].[CustomersCopy]

SELECT * INTO [Sales].[CustomersCopy]
FROM [Sales].[Customers] AS sc
WHERE 
	sc.[CustomerName] LIKE N'Tailspin Toys (San%'
	OR sc.[CustomerName] LIKE N'Tailspin Toys (East%'
	OR sc.[CustomerName] LIKE N'Tailspin Toys (North%';

-- Обновим поля Name и Phone для всех кроме Tailspin Toys (San
UPDATE [Sales].[CustomersCopy]
SET 
	[CustomerName] = REPLACE([CustomerName], 'North', 'South') 
	, [PhoneNumber] =  N'(701) 00 ' + CONVERT(NVARCHAR, [CustomerID])
OUTPUT 
    inserted.[CustomerName] as new_CustomerName
  , inserted.[PhoneNumber] as new_PhoneNumber
  , deleted.[CustomerName] as old_CustomerName
  , deleted.[PhoneNumber] old_PhoneNumber
WHERE [CustomerName] NOT LIKE N'Tailspin Toys (San%'

-- Обновим ID, name, phone для Tailspin Toys (San
UPDATE [Sales].[CustomersCopy]
SET 
	[CustomerName] = REPLACE([CustomerName], 'San', 'Saint') 
	, [CustomerID] = CONVERT(int, NEXT VALUE FOR [Sequences].[CustomerID])
	, [PhoneNumber] =  N'(701) 00 ' + CONVERT(NVARCHAR, [CustomerID])
OUTPUT 
    inserted.[CustomerID] as new_Id
  , inserted.[CustomerName] as new_CustomerName
  , inserted.[PhoneNumber] as new_PhoneNumber
  , deleted.[CustomerID] as old_Id
  , deleted.[CustomerName] as old_CustomerName
  , deleted.[PhoneNumber] old_PhoneNumber
WHERE [CustomerName] LIKE N'Tailspin Toys (San%'

--SELECT *
--FROM [Sales].[CustomersCopy]

SELECT * INTO [Sales].[CustomersFullCopy]
FROM [Sales].[Customers]

MERGE [Sales].[CustomersFullCopy] AS dst
USING [Sales].[CustomersCopy] AS src
    ON (dst.[CustomerID] = src.[CustomerID])
WHEN MATCHED 
    THEN UPDATE 
        SET 
			dst.[CustomerName] = src.[CustomerName]
			, dst.[PhoneNumber] = src.[PhoneNumber]
WHEN NOT MATCHED 
    THEN INSERT (
		CustomerID
		, CustomerName
		, BillToCustomerID
		, CustomerCategoryID
		, PrimaryContactPersonID
		, DeliveryMethodID
		, DeliveryCityID
		, PostalCityID
		, AccountOpenedDate
		, StandardDiscountPercentage
		, IsStatementSent
		, IsOnCreditHold
		, PaymentDays
		, PhoneNumber
		, FaxNumber
		, WebsiteURL
		, DeliveryAddressLine1
		, DeliveryPostalCode
		, PostalAddressLine1
		, PostalPostalCode
		, LastEditedBy
		, ValidFrom
		, ValidTo)
        VALUES (
			src.CustomerID
			, src.CustomerName
			, src.BillToCustomerID
			, src.CustomerCategoryID
			, src.PrimaryContactPersonID
			, src.DeliveryMethodID
			, src.DeliveryCityID
			, src.PostalCityID
			, src.AccountOpenedDate
			, src.StandardDiscountPercentage
			, src.IsStatementSent
			, src.IsOnCreditHold
			, src.PaymentDays
			, src.PhoneNumber
			, src.FaxNumber
			, src.WebsiteURL
			, src.DeliveryAddressLine1
			, src.DeliveryPostalCode
			, src.PostalAddressLine1
			, src.PostalPostalCode
			, src.LastEditedBy
			, src.ValidFrom
			, src.ValidTo)
OUTPUT deleted.*, $action, inserted.*;

/*
5. Напишите запрос, который выгрузит данные через bcp out и загрузить через bulk insert
*/

-- To allow advanced options to be changed.  
EXEC sp_configure 'show advanced options', 1;  
GO  
-- To update the currently configured value for advanced options.  
RECONFIGURE;  
GO  
-- To enable the feature.  
EXEC sp_configure 'xp_cmdshell', 1;  
GO  
-- To update the currently configured value for this feature.  
RECONFIGURE;  
GO

--SELECT @@SERVERNAME

--выгрузка CustomersCopy в Customers.csv
DECLARE @out varchar(250);
SET @out = 'bcp WideWorldImporters.Sales.CustomersCopy OUT "c:\temp\Customers.csv" -T -S ' + @@SERVERNAME + ' -q -c';
PRINT @out;

EXEC master..xp_cmdshell @out

--загрузка из Customers.csv в [CustomersCopy2]
DROP TABLE IF EXISTS [Sales].[CustomersCopy2]
SELECT * INTO [Sales].[CustomersCopy2]
FROM [Sales].[Customers] AS sc
WHERE 1=2

DECLARE @in varchar(250);
SET @in = 'bcp WideWorldImporters.Sales.CustomersCopy2 IN "c:\temp\Customers.csv" -T -S ' + @@SERVERNAME + ' -q -c';
PRINT @in;

EXEC master..xp_cmdshell @in

SELECT *
FROM [Sales].[CustomersCopy2]

-- To disable the feature.  
EXEC sp_configure 'xp_cmdshell', 0;  
GO  
-- To update the currently configured value for this feature.  
RECONFIGURE;  
GO 
-- To disallow advanced options to be changed.  
EXEC sp_configure 'show advanced options', 0;  
GO  
-- To update the currently configured value for advanced options.  
RECONFIGURE;  
GO

-- Удалим изменения
DROP TABLE IF EXISTS [Sales].[CustomersFullCopy]
DROP TABLE IF EXISTS [Sales].[CustomersCopy1]
DROP TABLE IF EXISTS [Sales].[CustomersCopy2]

DELETE FROM [Sales].[Customers]
WHERE [CustomerName] LIKE 'Customer%'