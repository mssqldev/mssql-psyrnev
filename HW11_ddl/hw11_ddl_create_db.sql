CREATE DATABASE DreamTeam ON
(NAME = Sales_dat,
    FILENAME = 'c:\Users\psyrnev\Projects\dotnet\MSSql\DreamTeam\DB\\dreamteam.mdf',
    SIZE = 10,
    FILEGROWTH = 5)
LOG ON
(NAME = Sales_log,
    FILENAME = 'c:\Users\psyrnev\Projects\dotnet\MSSql\DreamTeam\Log\dreamteamlog.ldf',
    SIZE = 5 MB,
    FILEGROWTH = 5 MB);
GO