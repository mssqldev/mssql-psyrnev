﻿USE DreamTeam;
/*
## Создание схемы
По ощущениям на данном этапе досточно одной схемы назовем ее `dt`
*/

--DROP SCHEMA IF EXISTS dt;
CREATE SCHEMA dt;


/*
### Club
*/

DROP TABLE IF EXISTS dt.[Club]
CREATE TABLE dt.[Club] (
    [ClubID] uniqueidentifier  NOT NULL DEFAULT NEWSEQUENTIALID(),
    [Title] NVARCHAR(100)  NOT NULL ,
    [Location] Geography  NOT NULL ,
    CONSTRAINT [PK_Club] PRIMARY KEY CLUSTERED (
        [ClubID] ASC
    )
)


/*
### Player
*/

DROP TABLE IF EXISTS dt.[Player]
CREATE TABLE dt.[Player] (
    [PlayerID] uniqueidentifier  NOT NULL DEFAULT NEWSEQUENTIALID(),
    [FirstName] NVARCHAR(50)  NOT NULL ,
    [SecondName] NVARCHAR(50)  NOT NULL ,
    [DateOfBirth] Date  NOT NULL ,
    [Num] int  NOT NULL ,
    -- C, LW, RW, D, G
    [Role] NVARCHAR(2)  NOT NULL ,
    -- L - left R - right
    [Shoots] NVARCHAR(1)  NOT NULL ,
    -- centimeter
    [Height] int  NOT NULL ,
    -- gramm
    [Weight] int  NOT NULL ,
    [Citizenship] NVARCHAR(50)  NOT NULL ,
    [FirstEventID] uniqueidentifier  NULL ,
    CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED (
        [PlayerID] ASC
    )
)

/*
### Связь между Player и Club

#### Таблица для реализации связзи N <-> M
*/

--DROP TABLE IF EXISTS dt.[b_Club_Player]
CREATE TABLE dt.[b_Club_Player] (
    [ClubID] uniqueidentifier  NOT NULL ,
    [PlayerID] uniqueidentifier  NOT NULL ,
    [StartedAt] date  NOT NULL ,
    [FinishedAt] date  NOT NULL 
)

/*
#### Внешние ключи
*/

ALTER TABLE dt.[b_Club_Player] WITH CHECK ADD CONSTRAINT [FK_b_Club_Player_ClubID] FOREIGN KEY([ClubID])
REFERENCES dt.[Club] ([ClubID])

ALTER TABLE dt.[b_Club_Player] CHECK CONSTRAINT [FK_b_Club_Player_ClubID]

ALTER TABLE dt.[b_Club_Player] WITH CHECK ADD CONSTRAINT [FK_b_Club_Player_PlayerID] FOREIGN KEY([PlayerID])
REFERENCES dt.[Player] ([PlayerID])

ALTER TABLE dt.[b_Club_Player] CHECK CONSTRAINT [FK_b_Club_Player_PlayerID]

/*
### Transfers
*/

DROP TABLE IF EXISTS dt.[Transfers]
CREATE TABLE dt.[Transfers] (
    -- sequential
    [TransferID] uniqueidentifier  NOT NULL DEFAULT NEWSEQUENTIALID(),
    [Date] datetime2(0)  NOT NULL ,
    [PlayerID] uniqueidentifier  NOT NULL ,
    [SourceClubID] uniqueidentifier ,
    [DestinationClubID] uniqueidentifier ,
    CONSTRAINT [PK_Transfers] PRIMARY KEY CLUSTERED (
        [TransferID] ASC
    )
)

/*
#### Внешние ключи
*/
ALTER TABLE dt.[Transfers] WITH CHECK ADD CONSTRAINT [FK_Transfers_PlayerID] FOREIGN KEY([PlayerID])
REFERENCES dt.[Player] ([PlayerID])

ALTER TABLE dt.[Transfers] CHECK CONSTRAINT [FK_Transfers_PlayerID]

ALTER TABLE dt.[Transfers] WITH CHECK ADD CONSTRAINT [FK_Transfers_SourceClubID] FOREIGN KEY([SourceClubID])
REFERENCES dt.[Club] ([ClubID])

ALTER TABLE dt.[Transfers] CHECK CONSTRAINT [FK_Transfers_SourceClubID]

ALTER TABLE dt.[Transfers] WITH CHECK ADD CONSTRAINT [FK_Transfers_DestinationClubID] FOREIGN KEY([DestinationClubID])
REFERENCES dt.[Club] ([ClubID])

ALTER TABLE dt.[Transfers] CHECK CONSTRAINT [FK_Transfers_DestinationClubID]


/*
### Schedule
*/

DROP TABLE IF EXISTS dt.[Schedule] 
CREATE TABLE dt.[Schedule] (
    -- sequential
    [ScheduleID] uniqueidentifier NOT NULL DEFAULT NEWSEQUENTIALID() ,
    [FirstDate] datetime2(0) NOT NULL ,
    [LastDate] datetime2(0) NOT NULL ,
    CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED (
        [ScheduleID] ASC
    )
)

/*
### Events
*/

DROP TABLE IF EXISTS dt.[Events]
CREATE TABLE dt.[Events] (
    -- sequential
    [EventID] uniqueidentifier NOT NULL DEFAULT NEWSEQUENTIALID() ,
    [EventType] int NOT NULL ,
    [Date] datetime2(0) NOT NULL ,
    [Details] NVARCHAR(200) NOT NULL ,
    [HomeClubApplicationID] uniqueidentifier NOT NULL ,
    [GuestClubApplicationID] uniqueidentifier NOT NULL ,
    [SheduleID] uniqueidentifier NOT NULL ,
    [Location] Geography NOT NULL ,
    CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED (
        [EventID] ASC
    )
)

/*
#### Внешние ключи
*/

ALTER TABLE dt.[Events] WITH CHECK ADD CONSTRAINT [FK_Events_SheduleID] FOREIGN KEY([SheduleID])
REFERENCES dt.[Schedule] ([ScheduleID])

ALTER TABLE dt.[Events] CHECK CONSTRAINT [FK_Events_SheduleID]

/*
### EventApplication
*/

DROP TABLE IF EXISTS dt.[EventApplication]
CREATE TABLE dt.[EventApplication] (
    -- sequential
    [ApplicationID] uniqueidentifier NOT NULL DEFAULT NEWSEQUENTIALID(),
    [ClubID] uniqueidentifier NOT NULL ,
    CONSTRAINT [PK_EventApplication] PRIMARY KEY CLUSTERED (
        [ApplicationID] ASC
    )
);
GO

DROP TABLE IF EXISTS dt.[EventApplicationLine]
CREATE TABLE dt.[EventApplicationLine] (
    -- sequential
    [EventApplicationLineID] uniqueidentifier NOT NULL DEFAULT NEWSEQUENTIALID(),
    [ApplicationID] uniqueidentifier NOT NULL ,
    [PlayerID] uniqueidentifier NOT NULL ,
    CONSTRAINT [PK_EventApplicationLine] PRIMARY KEY CLUSTERED (
        [EventApplicationLineID] ASC
    )
)


/*
### EventLog
*/

CREATE TABLE dt.[EventLog] (
    -- sequential
    [EventLogID] uniqueidentifier NOT NULL DEFAULT NEWSEQUENTIALID(),
    [EventID] uniqueidentifier NOT NULL ,
    [Time] time(3) NOT NULL ,
    [MeasurementID] unit NOT NULL ,
    [PlayerID] uniqueidentifier NOT NULL ,
    CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED (
        [EventLogID] ASC
    )
);
GO

CREATE TABLE dt.[Measurement] (
    [MeasurementID] int NOT NULL ,
    [Name] NVARCHAR(10) NOT NULL ,
    [Description] NVARCHAR(200) NOT NULL ,
    CONSTRAINT [PK_Measurement] PRIMARY KEY CLUSTERED (
        [MeasurementID] ASC
    )
)

/*
#### Внешние ключи
*/

ALTER TABLE dt.[EventLog] WITH CHECK ADD CONSTRAINT [FK_EventLog_EventID] FOREIGN KEY([EventID])
REFERENCES dt.[Events] ([EventID])

ALTER TABLE dt.[EventLog] CHECK CONSTRAINT [FK_EventLog_EventID]

ALTER TABLE dt.[EventLog] WITH CHECK ADD CONSTRAINT [FK_EventLog_MeasurementID] FOREIGN KEY([MeasurementID])
REFERENCES dt.[Measurement] ([MeasurementID])

ALTER TABLE dt.[EventLog] CHECK CONSTRAINT [FK_EventLog_MeasurementID]

ALTER TABLE dt.[EventLog] WITH CHECK ADD CONSTRAINT [FK_EventLog_PlayerID] FOREIGN KEY([PlayerID])
REFERENCES dt.[Player] ([PlayerID])

ALTER TABLE dt.[EventLog] CHECK CONSTRAINT [FK_EventLog_PlayerID]