﻿-- Exported from QuickDBD: https://www.quickdatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/d/Yt5yy2
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.

-- https://app.quickdatabasediagrams.com/

SET XACT_ABORT ON

BEGIN TRANSACTION QUICKDBD

CREATE TABLE [Club] (
    [ClubID] uniqueidentifier  NOT NULL ,
    [Title] NVARCHAR(100)  NOT NULL ,
    [Location] Geography  NOT NULL ,
    CONSTRAINT [PK_Club] PRIMARY KEY CLUSTERED (
        [ClubID] ASC
    )
)

CREATE TABLE [Player] (
    [PlayerID] uniqueidentifier  NOT NULL ,
    [FirstName] NVARCHAR(50)  NOT NULL ,
    [SecondName] NVARCHAR(50)  NOT NULL ,
    [DateOfBirth] Date  NOT NULL ,
    [Num] uint  NOT NULL ,
    -- C, LW, RW, D, G
    [Role] NVARCHAR(2)  NOT NULL ,
    -- 1 - left 2 - right
    [Shoots] unit  NOT NULL ,
    -- centimeter
    [Height] uint  NOT NULL ,
    -- gramm
    [Weight] uint  NOT NULL ,
    [Citizenship] NVARCHAR(50)  NOT NULL ,
    [FirstEventID] uniqueidentifier  NULL ,
    CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED (
        [PlayerID] ASC
    )
)

CREATE TABLE [b_Club_Player] (
    [ClubID] uniqueidentifier  NOT NULL ,
    [PlayerID] uniqueidentifier  NOT NULL ,
    [StartedAt] date  NOT NULL ,
    [FinishedAt] date  NOT NULL 
)

CREATE TABLE [Events] (
    -- sequential
    [EventID] uniqueidentifier  NOT NULL ,
    [EventType] int  NOT NULL ,
    [Date] datetime2(0)  NOT NULL ,
    [Details] NVARCHAR(200)  NOT NULL ,
    [HomeClubApplicationID] uniqueidentifier  NOT NULL ,
    [GuestClubApplicationID] uniqueidentifier  NOT NULL ,
    [SheduleID] uniqueidentifier  NOT NULL ,
    [Location] Geography  NOT NULL ,
    CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED (
        [EventID] ASC
    )
)

CREATE TABLE [Schedule] (
    -- sequential
    [ScheduleID] uniqueidentifier  NOT NULL ,
    [FirstDate] datetime2(0)  NOT NULL ,
    [LastDate] datetime2(0)  NOT NULL ,
    CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED (
        [ScheduleID] ASC
    )
)

CREATE TABLE [EventApplication] (
    -- sequential
    [ApplicationID] uniqueidentifier  NOT NULL ,
    [ClubID] uniqueidentifier  NOT NULL ,
    CONSTRAINT [PK_EventApplication] PRIMARY KEY CLUSTERED (
        [ApplicationID] ASC
    )
)

CREATE TABLE [EventApplicationLine] (
    -- sequential
    [EventApplicationLineID] uniqueidentifier  NOT NULL ,
    [ApplicationID] uniqueidentifier  NOT NULL ,
    [PlayerID] uniqueidentifier  NOT NULL ,
    CONSTRAINT [PK_EventApplicationLine] PRIMARY KEY CLUSTERED (
        [EventApplicationLineID] ASC
    )
)

CREATE TABLE [EventLog] (
    -- sequential
    [EventLogID] uniqueidentifier  NOT NULL ,
    [EventID] uniqueidentifier  NOT NULL ,
    [Time] time(3)  NOT NULL ,
    [MeasurementID] unit  NOT NULL ,
    [PlayerID] uniqueidentifier  NOT NULL ,
    CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED (
        [EventLogID] ASC
    )
)

CREATE TABLE [Measurement] (
    [MeasurementID] uniqueidentifier  NOT NULL ,
    [Name] NVARCHAR(10)  NOT NULL ,
    [Description] NVARCHAR(200)  NOT NULL ,
    CONSTRAINT [PK_Measurement] PRIMARY KEY CLUSTERED (
        [MeasurementID] ASC
    )
)

CREATE TABLE [Transfers] (
    -- sequential
    [TransferID] uniqueidentifier  NOT NULL ,
    [Date] datetime2(0)  NOT NULL ,
    [PlayerID] uniqueidentifier  NOT NULL ,
    [SourceClubID] uniqueidentifier  NULL ,
    [DestinationClubID] uniqueidentifier  NULL ,
    CONSTRAINT [PK_Transfers] PRIMARY KEY CLUSTERED (
        [TransferID] ASC
    )
)

ALTER TABLE [Player] WITH CHECK ADD CONSTRAINT [FK_Player_FirstEventID] FOREIGN KEY([FirstEventID])
REFERENCES [Events] ([EventID])

ALTER TABLE [Player] CHECK CONSTRAINT [FK_Player_FirstEventID]

ALTER TABLE [b_Club_Player] WITH CHECK ADD CONSTRAINT [FK_b_Club_Player_ClubID] FOREIGN KEY([ClubID])
REFERENCES [Club] ([ClubID])

ALTER TABLE [b_Club_Player] CHECK CONSTRAINT [FK_b_Club_Player_ClubID]

ALTER TABLE [b_Club_Player] WITH CHECK ADD CONSTRAINT [FK_b_Club_Player_PlayerID] FOREIGN KEY([PlayerID])
REFERENCES [Player] ([PlayerID])

ALTER TABLE [b_Club_Player] CHECK CONSTRAINT [FK_b_Club_Player_PlayerID]

ALTER TABLE [Events] WITH CHECK ADD CONSTRAINT [FK_Events_HomeClubApplicationID] FOREIGN KEY([HomeClubApplicationID])
REFERENCES [EventApplication] ([ApplicationID])

ALTER TABLE [Events] CHECK CONSTRAINT [FK_Events_HomeClubApplicationID]

ALTER TABLE [Events] WITH CHECK ADD CONSTRAINT [FK_Events_GuestClubApplicationID] FOREIGN KEY([GuestClubApplicationID])
REFERENCES [EventApplication] ([ApplicationID])

ALTER TABLE [Events] CHECK CONSTRAINT [FK_Events_GuestClubApplicationID]

ALTER TABLE [Events] WITH CHECK ADD CONSTRAINT [FK_Events_SheduleID] FOREIGN KEY([SheduleID])
REFERENCES [Schedule] ([ScheduleID])

ALTER TABLE [Events] CHECK CONSTRAINT [FK_Events_SheduleID]

ALTER TABLE [EventApplication] WITH CHECK ADD CONSTRAINT [FK_EventApplication_ClubID] FOREIGN KEY([ClubID])
REFERENCES [Club] ([ClubID])

ALTER TABLE [EventApplication] CHECK CONSTRAINT [FK_EventApplication_ClubID]

ALTER TABLE [EventApplicationLine] WITH CHECK ADD CONSTRAINT [FK_EventApplicationLine_ApplicationID] FOREIGN KEY([ApplicationID])
REFERENCES [EventApplication] ([ApplicationID])

ALTER TABLE [EventApplicationLine] CHECK CONSTRAINT [FK_EventApplicationLine_ApplicationID]

ALTER TABLE [EventApplicationLine] WITH CHECK ADD CONSTRAINT [FK_EventApplicationLine_PlayerID] FOREIGN KEY([PlayerID])
REFERENCES [Player] ([PlayerID])

ALTER TABLE [EventApplicationLine] CHECK CONSTRAINT [FK_EventApplicationLine_PlayerID]

ALTER TABLE [EventLog] WITH CHECK ADD CONSTRAINT [FK_EventLog_EventID] FOREIGN KEY([EventID])
REFERENCES [Events] ([EventID])

ALTER TABLE [EventLog] CHECK CONSTRAINT [FK_EventLog_EventID]

ALTER TABLE [EventLog] WITH CHECK ADD CONSTRAINT [FK_EventLog_MeasurementID] FOREIGN KEY([MeasurementID])
REFERENCES [Measurement] ([MeasurementID])

ALTER TABLE [EventLog] CHECK CONSTRAINT [FK_EventLog_MeasurementID]

ALTER TABLE [EventLog] WITH CHECK ADD CONSTRAINT [FK_EventLog_PlayerID] FOREIGN KEY([PlayerID])
REFERENCES [Player] ([PlayerID])

ALTER TABLE [EventLog] CHECK CONSTRAINT [FK_EventLog_PlayerID]

ALTER TABLE [Transfers] WITH CHECK ADD CONSTRAINT [FK_Transfers_PlayerID] FOREIGN KEY([PlayerID])
REFERENCES [Player] ([PlayerID])

ALTER TABLE [Transfers] CHECK CONSTRAINT [FK_Transfers_PlayerID]

ALTER TABLE [Transfers] WITH CHECK ADD CONSTRAINT [FK_Transfers_SourceClubID] FOREIGN KEY([SourceClubID])
REFERENCES [Club] ([ClubID])

ALTER TABLE [Transfers] CHECK CONSTRAINT [FK_Transfers_SourceClubID]

ALTER TABLE [Transfers] WITH CHECK ADD CONSTRAINT [FK_Transfers_DestinationClubID] FOREIGN KEY([DestinationClubID])
REFERENCES [Club] ([ClubID])

ALTER TABLE [Transfers] CHECK CONSTRAINT [FK_Transfers_DestinationClubID]

COMMIT TRANSACTION QUICKDBD