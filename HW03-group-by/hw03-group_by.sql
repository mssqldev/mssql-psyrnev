/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.
Занятие "02 - Оператор SELECT и простые фильтры, GROUP BY, HAVING".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
1. Посчитать среднюю цену товара, общую сумму продажи по месяцам.
Вывести:
* Год продажи (например, 2015)
* Месяц продажи (например, 4)
* Средняя цена за месяц по всем товарам
* Общая сумма продаж за месяц

Продажи смотреть в таблице Sales.Invoices и связанных таблицах.
*/

SELECT
	DATEPART(YEAR, si.[InvoiceDate]) AS [Year]
	, DATEPART(MONTH, si.[InvoiceDate]) AS [Month]
	, AVG(sil.[UnitPrice] * sil.[Quantity]) AS MeanPrice
	, SUM(sil.[UnitPrice] * sil.[Quantity]) AS TotalSum
FROM [Sales].[Invoices] AS si
	JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
GROUP BY DATEPART(YEAR, si.[InvoiceDate]), DATEPART(MONTH, si.[InvoiceDate])
ORDER BY 1,2,3

/*
2. Отобразить все месяцы, где общая сумма продаж превысила 4 600 000

Вывести:
* Год продажи (например, 2015)
* Месяц продажи (например, 4)
* Общая сумма продаж

Продажи смотреть в таблице Sales.Invoices и связанных таблицах.
*/

SELECT
	DATEPART(YEAR, si.[InvoiceDate]) AS [Year]
	, DATEPART(MONTH, si.[InvoiceDate]) AS [Month]
	, SUM(sil.[UnitPrice] * sil.[Quantity]) AS TotalSum
FROM [Sales].[Invoices] AS si
	JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
GROUP BY DATEPART(YEAR, si.[InvoiceDate]), DATEPART(MONTH, si.[InvoiceDate])
HAVING SUM(sil.[UnitPrice] * sil.[Quantity]) > 4600000
ORDER BY 1,2

/*
3. Вывести сумму продаж, дату первой продажи
и количество проданного по месяцам, по товарам,
продажи которых менее 50 ед в месяц.
Группировка должна быть по году,  месяцу, товару.

Вывести:
* Год продажи
* Месяц продажи
* Наименование товара
* Сумма продаж
* Дата первой продажи
* Количество проданного

Продажи смотреть в таблице Sales.Invoices и связанных таблицах.
*/

/* Выборка для случая `Дата первой продажи` - это дата первой вообще продажи */
; WITH firstDateSaleCte AS
-- выборка [StockItemID] [StockItemName] [FirstSaleDate] для получения даты первой продажи
(
	SELECT
		sil.[StockItemID]
		, wsi.[StockItemName]
		, MIN(si.[InvoiceDate]) AS FirstSaleDate
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
		JOIN [Warehouse].[StockItems] AS wsi ON sil.[StockItemID] = wsi.[StockItemID]
	GROUP BY sil.[StockItemID], wsi.[StockItemName]
	--ORDER BY 1
)
, mainCte AS
-- основная выборка в разрезе Год, Месяц, Товар
(
	SELECT
		sil.[StockItemID]
		, DATEPART(YEAR, si.[InvoiceDate]) AS [Year]
		, DATEPART(MONTH, si.[InvoiceDate]) AS [Month]
		, SUM(sil.[UnitPrice] * sil.[Quantity]) AS TotalSum
		, SUM(sil.[Quantity]) AS [Quantity]
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	GROUP BY 
		DATEPART(YEAR, si.[InvoiceDate])
		, DATEPART(MONTH, si.[InvoiceDate])
		, sil.[StockItemID]
)
SELECT
	mainCte.[Year]
	, mainCte.[Month]
	, fdsCte.[StockItemName]
	, mainCte.TotalSum
	, fdsCte.FirstSaleDate
	, mainCte.Quantity
FROM firstDateSaleCte AS fdsCte
	JOIN mainCte ON fdsCte.[StockItemID] = mainCte.[StockItemID]
--WHERE mainCte.[StockItemID] = 1
ORDER BY 1,2, [TotalSum] DESC

/* Выборка для случая `Дата первой продажи` - это дата первой продажи в текущем месяце*/
; WITH mainCte AS
-- основная выборка в разрезе Год, Месяц, Товар
(
	SELECT
		sil.[StockItemID]
		, DATEPART(YEAR, si.[InvoiceDate]) AS [Year]
		, DATEPART(MONTH, si.[InvoiceDate]) AS [Month]
		, MIN(si.[InvoiceDate]) AS [FirstSaleDateInMonth]
		, SUM(sil.[UnitPrice] * sil.[Quantity]) AS TotalSum
		, SUM(sil.[Quantity]) AS [Quantity]
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	GROUP BY 
		DATEPART(YEAR, si.[InvoiceDate])
		, DATEPART(MONTH, si.[InvoiceDate])
		, sil.[StockItemID]
)
SELECT
	mainCte.[Year]
	, mainCte.[Month]
	, wsi.[StockItemName]
	, mainCte.TotalSum
	, mainCte.FirstSaleDateInMonth
	, mainCte.Quantity
FROM mainCte
	JOIN [Warehouse].[StockItems] AS wsi ON mainCte.[StockItemID] = wsi.[StockItemID]
--WHERE mainCte.[StockItemID] = 1
ORDER BY 1,2, [TotalSum] DESC

-- ---------------------------------------------------------------------------
-- Опционально
-- ---------------------------------------------------------------------------
/*
Написать запросы 2-3 так, чтобы если в каком-то месяце не было продаж,
то этот месяц также отображался бы в результатах, но там были нули.
*/

/* 2ой запрос */
-- генерим номера месяцев
;WITH monthsCte(MonthNum) AS
(
    SELECT 1
    UNION ALL
    SELECT MonthNum+1 
    FROM monthsCte
    WHERE MonthNum < 12
)
-- выбираем все года
, invoiceYearsCte AS
(
	SELECT DISTINCT DATEPART(YEAR, si.[InvoiceDate]) AS [Year]
	FROM [Sales].[Invoices] AS si
)
-- перемножаем года Х месяцы
, yearMonthCte AS
(
	SELECT iyCte.[Year], mCte.[MonthNum] AS [Month]
	FROM monthsCte AS mCte
		CROSS JOIN invoiceYearsCte AS iyCte
)
-- основной запрос
, sourceCte AS 
(
	SELECT
		DATEPART(YEAR, si.[InvoiceDate]) AS [Year]
		, DATEPART(MONTH, si.[InvoiceDate]) AS [Month]
		, SUM(sil.[UnitPrice] * sil.[Quantity]) AS TotalSum
	FROM [Sales].[Invoices] AS si
		JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
	GROUP BY DATEPART(YEAR, si.[InvoiceDate]), DATEPART(MONTH, si.[InvoiceDate])
	HAVING SUM(sil.[UnitPrice] * sil.[Quantity]) > 4600000
)
-- собираем через LEFT JOIN
SELECT
	ymCte.[Year]
	, ymCte.[Month]
	, sCte.TotalSum
FROM yearMonthCte AS ymCte
	LEFT JOIN sourceCte AS sCte ON 
		ymCte.[Year] = sCte.[Year] AND ymCte.[Month] = sCte.[Month]
ORDER BY 1,2

/*3ий запрос*/
;WITH monthsCte(MonthNum) AS
(
    SELECT 1
    UNION ALL
    SELECT MonthNum+1 
    FROM monthsCte
    WHERE MonthNum < 12
)
-- выбираем все года
, invoiceYearsCte AS
(
	SELECT DISTINCT DATEPART(YEAR, si.[InvoiceDate]) AS [Year]
	FROM [Sales].[Invoices] AS si
)
-- перемножаем года Х месяцы
, yearMonthCte AS
(
	SELECT iyCte.[Year], mCte.[MonthNum] AS [Month]
	FROM monthsCte AS mCte
		CROSS JOIN invoiceYearsCte AS iyCte
)
SELECT
	ymCte.[Year]
	, ymCte.[Month]
	, (
		SELECT wsi.[StockItemName]
		FROM [Warehouse].[StockItems] AS wsi 
		WHERE wsi.StockItemID = sil.[StockItemID]
		) AS StockName
	, SUM(sil.[UnitPrice] * sil.[Quantity]) AS TotalSum
	, MIN(si.[InvoiceDate]) AS [First]
	, SUM(sil.[Quantity]) AS [Quantity]
FROM yearMonthCte AS ymCte
	LEFT JOIN [Sales].[Invoices] AS si ON
		ymCte.[Year] = DATEPART(YEAR, si.[InvoiceDate]) 
		AND ymCte.[Month] = DATEPART(MONTH, si.[InvoiceDate])
	LEFT JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
GROUP BY 
	ymCte.[Year]
	, ymCte.[Month]
	, sil.[StockItemID]
ORDER BY 1, 2, [TotalSum] DESC