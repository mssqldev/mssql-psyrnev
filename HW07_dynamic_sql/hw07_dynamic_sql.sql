/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "07 - Динамический SQL".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*

Это задание из занятия "Операторы CROSS APPLY, PIVOT, UNPIVOT."
Нужно для него написать динамический PIVOT, отображающий результаты по всем клиентам.
Имя клиента указывать полностью из поля CustomerName.

Требуется написать запрос, который в результате своего выполнения 
формирует сводку по количеству покупок в разрезе клиентов и месяцев.
В строках должны быть месяцы (дата начала месяца), в столбцах - клиенты.

Дата должна иметь формат dd.mm.yyyy, например, 25.12.2019.

Пример, как должны выглядеть результаты:
-------------+--------------------+--------------------+----------------+----------------------
InvoiceMonth | Aakriti Byrraju    | Abel Spirlea       | Abel Tatarescu | ... (другие клиенты)
-------------+--------------------+--------------------+----------------+----------------------
01.01.2013   |      3             |        1           |      4         | ...
01.02.2013   |      7             |        3           |      4         | ...
-------------+--------------------+--------------------+----------------+----------------------
*/

-- Переменные для сборки запроса
DECLARE @ListToPivot NVARCHAR(max) = '',
		@query NVARCHAR(max) = ''

-- Список колонок для IN 
SELECT
	-- нельзя использовать STRING_AGG т.к. он ограничен 8000  
	@ListToPivot = CONCAT(
		@ListToPivot
		, QUOTENAME(sc.[CustomerName])
		, ', '
	)
FROM [Sales].[Customers] AS sc
--WHERE sc.[CustomerID] BETWEEN 2 AND 6 -- для тестов

-- отрезаем последний разделитель ', '
SET @ListToPivot = (SELECT RTRIM(@ListToPivot, ', '));
-- выводим полученный список
SELECT @ListToPivot AS CustomerList;

--соборка запроса
SET @query =N'
SELECT
	FORMAT([InvoiceMonth], ''dd.MM.yyyy'') AS [InvoiceMonth]
	, ' + @ListToPivot + N'
FROM (
	SELECT
		sc.[CustomerName]
		, DATETRUNC(MONTH, si.[InvoiceDate]) AS [InvoiceMonth]
		, si.[InvoiceID]
	FROM [Sales].[Customers] AS sc
		JOIN [Sales].[Invoices] si ON sc.[CustomerID] = si.[CustomerID]
	) AS subQ
	PIVOT 
	(
		COUNT(subQ.[InvoiceID])
		FOR subQ.[CustomerName]
		IN (' + @ListToPivot + N')
	) AS p
';
-- выводим полученный запрос
SELECT @query AS [Query];

-- выполняем запрос
EXEC (@query);