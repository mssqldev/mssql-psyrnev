# Постановка задачи

## Что есть:
Организация хоккейных непрофессиональных матчей (двусторонка). Сейчас есть несколько табличек в гулодоках, скрипт который парсит входной список отметивших игроков, на выходе составы команд. Флоу:
- список игроков чаще игравших за команду 1
- список игроков чаще игравших за команду 2
- на входе: список отметивших игроков
- на выходе: составы команд

## Проблемы:
- сложно добавить команду
- excel-специфическая работа с данными (list!А1:A50 и т.д)
- нет статистики по игрокам (не хватает для балансировки сил команд)
- фиксация результатов в другой гуглодоке

## Что хочется:
 1. упростить добавление команды
 2. собрать статистику по игрокам
 3. формировать сбалансированные команды для двусторонок
 4. формировать отчеты за месяц
    - по игрокам
    - по командам

# Нагрузка (на примере непрофисиональных лиг)
1. Количество команд < **100**
2. Количество игроков ~30 на команду, итого **300**
3. Количество матчей 576 + 65 = **641** (с сентября по март + плей офф)
<details>
    <summary><font color="gray">Расчет п3</font></summary>
    <pre>
    - за выходные: 10-12
    - за месяц: 8 * 12 = 96
    -  за сезон: 96 * 6 = 576
    - игры плей офф 65 игр
        - 16 команд (до 3 побед т.е. максимум 5 матчей)
        - 1 этап - 8 пар = 40 матчей максимум
        - 2 этап - 4 пары = 20 матчей максимум
        - 3 этап - 1 пара = 5 матчей
    </pre>
</details>

4. В каждом матче фиксируются события, пасы, голы, сейвы , начало, конец, нарушения и пр. Для оценки среднего количества событий возьмем одно событие в 5 секуд, для оценки максимума возьмем одно событие в секунду. Итого за сезон может накопиться **246 144 000 - 1 476 864 000**

<details>
    <summary><font color="gray">Расчет п4</font></summary>
    <pre>
    - матч длится 3600 секунд чистого времени + овертаймы + буллиты
        - количество событий за матч ~800-4000:
            - основное время: 720-3600
            - овертайм 5 минут: 60-300
            - буллиты: 10-20
        - Количество событий за игровой день:
            - 5-6 матчей за день
            - 4000 - 24000
        - Количество событий за игровой месяц:
            - 96 * 4000 - 24000 = 384 000 - 2 304 000
        - Количество событий за игровой сезон
            - 641 * 384 000 - 2 304 000 = 246 144 000 - 1 476 864 000
    </pre>
</details>

# Проектирование

## Сущности
- Игрок (фио, #, текущий клуб)
- Клуб (команда ранее) (название, локация)
- Календарь
- Событие (матч)
- Лог событий (статистика)
- Заявка клуба на событие содержит сисок игроков которые будут принимать участие
- Опционально:
    - Лог перехода игроков из клуба в клуб
    - Информация о травмированых игроках
    - Клубный стдаион (Название, локация, кол-во мест)

## Отношения
- Игрок может `играть` за несколько Клубов
- Календарь `состоит` из Событий
- 2 Клуба должны `подать` Заявки на участие в Событии
- Во время События `фиксируется` Статистика

## ER-диаграмма
<details>
    <summary><font color="gray">Исходник для <a href="https://app.quickdatabasediagrams.com">app.quickdatabasediagrams.com</font></a>
    </summary>
    <pre>
#https://app.quickdatabasediagrams.com/#/

Club
-
ClubID PK uniqueidentifier
Title NVARCHAR(100)
Location Geography


Player
-
PlayerID PK uniqueidentifier
FirstName NVARCHAR(50)
SecondName NVARCHAR(50)
DateOfBirth Date
Num uint
Role NVARCHAR(2) #C, LW, RW, D, G
Shoots unit #1 - left 2 - right 
Height uint #centimeter
Weight uint #gramm
Citizenship NVARCHAR(50)
FirstEventID NULL uniqueidentifier FK - Events.EventID


b_Club_Player
-
ClubID uniqueidentifier FK >- Club.ClubID
PlayerID uniqueidentifier FK >- Player.PlayerID
StartedAt date
FinishedAt date


Events
-
EventID uniqueidentifier PK #sequential
EventType int
Date datetime2(0)
Details NVARCHAR(200)
HomeClubApplicationID uniqueidentifier FK - EventApplication.ApplicationID
GuestClubApplicationID uniqueidentifier FK - EventApplication.ApplicationID
SheduleID uniqueidentifier FK >- Schedule.ScheduleID
Location Geography

Schedule
-
ScheduleID uniqueidentifier PK #sequential
FirstDate datetime2(0)
LastDate datetime2(0)

EventApplication
-
ApplicationID uniqueidentifier PK #sequential
ClubID uniqueidentifier FK - Club.ClubID

EventApplicationLine
-
EventApplicationLineID uniqueidentifier PK #sequential
ApplicationID uniqueidentifier FK >- EventApplication.ApplicationID
PlayerID uniqueidentifier FK >- Player.PlayerID

EventLog
-
EventLogID uniqueidentifier PK #sequential
EventID uniqueidentifier FK >- Events.EventID
Time time(3)
MeasurementID unit FK >- Measurement.MeasurementID
PlayerID uniqueidentifier FK >0- Player.PlayerID

Measurement
-
MeasurementID uniqueidentifier PK
Name NVARCHAR(10)
Description NVARCHAR(200)

Transfers
-
TransferID uniqueidentifier PK #sequential
Date datetime2(0)
PlayerID uniqueidentifier FK >- Player.PlayerID
SourceClubID NULL uniqueidentifier FK >- Club.ClubID
DestinationClubID NULL uniqueidentifier FK >- Club.ClubID
    </pre>
</details>

![QuickDBD](./QuickDBD-export.png)


