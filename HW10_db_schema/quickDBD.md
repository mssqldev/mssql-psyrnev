<details>
    <summary><font color="gray">Исходник для <a href="https://app.quickdatabasediagrams.com">app.quickdatabasediagrams.com</font></a>
    </summary>
    <pre>
#https://app.quickdatabasediagrams.com/#/

Club
-
ClubID PK uniqueidentifier
Title NVARCHAR(100)
Location Geography


Player
-
PlayerID PK uniqueidentifier
FirstName NVARCHAR(50)
SecondName NVARCHAR(50)
DateOfBirth Date
Num uint
Role NVARCHAR(2) #C, LW, RW, D, G
Shoots unit #1 - left 2 - right 
Height uint #centimeter
Weight uint #gramm
Citizenship NVARCHAR(50)
FirstEventID NULL uniqueidentifier FK - Events.EventID


b_Club_Player
-
ClubID uniqueidentifier FK >- Club.ClubID
PlayerID uniqueidentifier FK >- Player.PlayerID
StartedAt date
FinishedAt date


Events
-
EventID uniqueidentifier PK #sequential
EventType int
Date datetime2(0)
Details NVARCHAR(200)
HomeClubApplicationID uniqueidentifier FK - EventApplication.ApplicationID
GuestClubApplicationID uniqueidentifier FK - EventApplication.ApplicationID
SheduleID uniqueidentifier FK >- Schedule.ScheduleID
Location Geography

Schedule
-
ScheduleID uniqueidentifier PK #sequential
FirstDate datetime2(0)
LastDate datetime2(0)

EventApplication
-
ApplicationID uniqueidentifier PK #sequential
ClubID uniqueidentifier FK - Club.ClubID

EventApplicationLine
-
EventApplicationLineID uniqueidentifier PK #sequential
ApplicationID uniqueidentifier FK >- EventApplication.ApplicationID
PlayerID uniqueidentifier FK >- Player.PlayerID

EventLog
-
EventLogID uniqueidentifier PK #sequential
EventID uniqueidentifier FK >- Events.EventID
Time time(3)
MeasurementID unit FK >- Measurement.MeasurementID
PlayerID uniqueidentifier FK >0- Player.PlayerID

Measurement
-
MeasurementID uniqueidentifier PK
Name NVARCHAR(10)
Description NVARCHAR(200)

Transfers
-
TransferID uniqueidentifier PK #sequential
Date datetime2(0)
PlayerID uniqueidentifier FK >- Player.PlayerID
SourceClubID NULL uniqueidentifier FK >- Club.ClubID
DestinationClubID NULL uniqueidentifier FK >- Club.ClubID
    </pre>
</details>