﻿/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.
Занятие "02 - Оператор SELECT и простые фильтры, JOIN".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД WideWorldImporters можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/download/wide-world-importers-v1.0/WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
1. Все товары, в названии которых есть "urgent" или название начинается с "Animal".
Вывести: ИД товара (StockItemID), наименование товара (StockItemName).
Таблицы: Warehouse.StockItems.
*/

SELECT [StockItemId], [StockItemName]
FROM [Warehouse].[StockItems]
WHERE [StockItemName] LIKE '%urgent%' OR [StockItemName] LIKE 'Animal%'

/*
2. Поставщиков (Suppliers), у которых не было сделано ни одного заказа (PurchaseOrders).
Сделать через JOIN, с подзапросом задание принято не будет.
Вывести: ИД поставщика (SupplierID), наименование поставщика (SupplierName).
Таблицы: Purchasing.Suppliers, Purchasing.PurchaseOrders.
По каким колонкам делать JOIN подумайте самостоятельно.
*/

SELECT s.[SupplierID], s.[SupplierName], po.SupplierID
FROM 
	[Purchasing].[Suppliers] AS s
	LEFT JOIN [Purchasing].[PurchaseOrders] AS po ON s.[SupplierID] = po.[SupplierID]
WHERE po.[SupplierID] IS NULL
	
/*
3. Заказы (Orders) с ценой товара (UnitPrice) более 100$ 
либо количеством единиц (Quantity) товара более 20 штук
и присутствующей датой комплектации всего заказа (PickingCompletedWhen).
Вывести:
* OrderID
* дату заказа (OrderDate) в формате ДД.ММ.ГГГГ
* название месяца, в котором был сделан заказ
* номер квартала, в котором был сделан заказ
* треть года, к которой относится дата заказа (каждая треть по 4 месяца)
* имя заказчика (Customer)
Добавьте вариант этого запроса с постраничной выборкой,
пропустив первую 1000 и отобразив следующие 100 записей.

Сортировка должна быть по номеру квартала, трети года, дате заказа (везде по возрастанию).

Таблицы: Sales.Orders, Sales.OrderLines, Sales.Customers.
*/

-- Выборка целиком (без педжинга)
SELECT 
	o.[OrderID]
	, FORMAT( o.[OrderDate], 'dd.MM.yyyy') AS [OrderDate]
	, DATENAME(month, o.[OrderDate]) AS [Month]
	, DATEPART(quarter, o.[OrderDate]) AS [Quarter]
	, ((DATEPART(month, o.[OrderDate]) - 1) / 4 ) + 1 AS [A third]
	, c.[CustomerName] AS [CustomerName]
FROM [Sales].[Orders] as o
	JOIN [Sales].[OrderLines] AS ol ON o.[OrderID] = ol.[OrderID]
	JOIN [Sales].[Customers] AS c ON o.[CustomerID] = c.[CustomerID]
WHERE o.[PickingCompletedWhen] IS NOT NULL 
	AND (ol.[UnitPrice] > 100 OR ol.[Quantity] > 20)
ORDER BY [Quarter], [A third], [OrderDate]

-- построничная выборка
DECLARE 
	@page_size BIGINT = 100
	, @current_page BIGINT = 11;
SELECT 
	o.[OrderID]
	, FORMAT( o.[OrderDate], 'dd.MM.yyyy') AS [OrderDate]
	, DATENAME(month, o.[OrderDate]) AS [Month]
	, DATEPART(quarter, o.[OrderDate]) AS [Quarter]
	, ((DATEPART(month, o.[OrderDate]) - 1) / 4 ) + 1 AS [A third]
	, c.[CustomerName] AS [CustomerName] 
FROM [Sales].[Orders] as o
	JOIN [Sales].[OrderLines] AS ol ON o.[OrderID] = ol.[OrderID]
	JOIN [Sales].[Customers] AS c ON o.[CustomerID] = c.[CustomerID]
WHERE o.[PickingCompletedWhen] IS NOT NULL 
	AND (ol.[UnitPrice] > 100 OR ol.[Quantity] > 20)
ORDER BY [Quarter], [A third], [OrderDate] OFFSET(@current_page - 1) * @page_size ROWS
FETCH NEXT @page_size ROWS ONLY

/*
4. Заказы поставщикам (Purchasing.Suppliers),
которые должны быть исполнены (ExpectedDeliveryDate) в январе 2013 года
с доставкой "Air Freight" или "Refrigerated Air Freight" (DeliveryMethodName)
и которые исполнены (IsOrderFinalized).
Вывести:
* способ доставки (DeliveryMethodName)
* дата доставки (ExpectedDeliveryDate)
* имя поставщика
* имя контактного лица принимавшего заказ (ContactPerson)

Таблицы: Purchasing.Suppliers, Purchasing.PurchaseOrders, Application.DeliveryMethods, Application.People.
*/

SELECT
	adm.[DeliveryMethodName]
	, subPpo.[ExpectedDeliveryDate]
	, ps.[SupplierName]
	, ap.[FullName] AS [ContactPerson]
FROM 
	(
		SELECT
			ppo.[PurchaseOrderID]
			, ppo.[SupplierID]
			, ppo.[ExpectedDeliveryDate]
			, ppo.[DeliveryMethodID]
		FROM [Purchasing].[PurchaseOrders] AS ppo
		WHERE ppo.[ExpectedDeliveryDate]  BETWEEN '2013.01.01' AND '2013.01.31'
			AND ppo.[IsOrderFinalized] = 1
	) AS  subPpo
	JOIN [Purchasing].[Suppliers] AS ps ON subPpo.[SupplierID] = ps.[SupplierID]
	JOIN [Application].[People] AS ap ON ps.[PrimaryContactPersonID] = ap.[PersonID]
	JOIN [Application].[DeliveryMethods] adm ON subPpo.[DeliveryMethodID] = adm.[DeliveryMethodID]

SELECT
	adm.[DeliveryMethodName]
	, subPpo.[ExpectedDeliveryDate]
	, ps.[SupplierName]
	, ap.[FullName] AS [ContactPerson]
FROM [Purchasing].[PurchaseOrders] AS  subPpo
	JOIN [Purchasing].[Suppliers] AS ps ON subPpo.[SupplierID] = ps.[SupplierID]
	JOIN [Application].[People] AS ap ON ps.[PrimaryContactPersonID] = ap.[PersonID]
	JOIN [Application].[DeliveryMethods] adm ON subPpo.[DeliveryMethodID] = adm.[DeliveryMethodID]
WHERE subPpo.[ExpectedDeliveryDate]  BETWEEN '2013.01.01' AND '2013.01.31'
	 AND subPpo.[IsOrderFinalized] = 1

/*
5. Десять последних продаж (по дате продажи) с именем клиента и именем сотрудника,
который оформил заказ (SalespersonPerson).
Сделать без подзапросов.
*/

--SELECT TOP 10 WITH TIES
SELECT TOP 10
	so.OrderID
	, so.OrderDate
	, sc.[CustomerName]
	, ap.[FullName]
FROM [Sales].[Orders] AS so
	JOIN [Sales].[Customers] AS sc ON so.[CustomerID] = sc.[CustomerID]
	JOIN [Application].[People] AS ap ON so.[SalespersonPersonID] = ap.[PersonID]
ORDER BY so.[OrderDate] DESC

/*
6. Все ид и имена клиентов и их контактные телефоны,
которые покупали товар "Chocolate frogs 250g".
Имя товара смотреть в таблице Warehouse.StockItems.
*/

SELECT DISTINCT
	sc.[CustomerID]
	, sc.[CustomerName]
	, sc.[PhoneNumber]
FROM [Sales].[Customers] AS sc 
	JOIN [Sales].[Orders] AS so ON sc.[CustomerID] = so.[CustomerID]
	JOIN [Sales].[OrderLines] AS sol ON so.[OrderID] = sol.[OrderID]
	JOIN [Warehouse].[StockItems] AS wsi ON sol.[StockItemID] = wsi.[StockItemID] 
WHERE wsi.[StockItemName] = 'Chocolate frogs 250g'
ORDER BY 1
