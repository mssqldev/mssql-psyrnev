CREATE DATABASE DreamTeamDW ON
(NAME = dream_team_dw_dat,
    FILENAME = 'c:\Users\psyrnev\Projects\dotnet\MSSql\DreamTeamDW\DB\\teammanager.mdf',
    SIZE = 10,
    FILEGROWTH = 5)
LOG ON
(NAME = dream_team_dw_log,
    FILENAME = 'c:\Users\psyrnev\Projects\dotnet\MSSql\DreamTeamDW\Log\teammanagerlog.ldf',
    SIZE = 5 MB,
    FILEGROWTH = 5 MB);

GO
USE DreamTeamDW;
GO
--DROP SCHEMA IF EXISTS Dimension;
--CREATE SCHEMA Dimension;
--GO
--CREATE SCHEMA Fact;
--GO