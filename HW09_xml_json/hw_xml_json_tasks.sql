/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "08 - Выборки из XML и JSON полей".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
Примечания к заданиям 1, 2:
* Если с выгрузкой в файл будут проблемы, то можно сделать просто SELECT c результатом в виде XML. 
* Если у вас в проекте предусмотрен экспорт/импорт в XML, то можете взять свой XML и свои таблицы.
* Если с этим XML вам будет скучно, то можете взять любые открытые данные и импортировать их в таблицы (например, с https://data.gov.ru).
* Пример экспорта/импорта в файл https://docs.microsoft.com/en-us/sql/relational-databases/import-export/examples-of-bulk-import-and-export-of-xml-documents-sql-server
*/


/*
1. В личном кабинете есть файл StockItems.xml.
Это данные из таблицы Warehouse.StockItems.
Преобразовать эти данные в плоскую таблицу с полями, аналогичными Warehouse.StockItems.
Поля: StockItemName, SupplierID, UnitPackageID, OuterPackageID, QuantityPerOuter, TypicalWeightPerUnit, LeadTimeDays, IsChillerStock, TaxRate, UnitPrice 

Загрузить эти данные в таблицу Warehouse.StockItems: 
существующие записи в таблице обновить, отсутствующие добавить (сопоставлять записи по полю StockItemName). 

Сделать два варианта: с помощью OPENXML и через XQuery.
*/

DECLARE
	@x xml
	, @hdoc int

SELECT @x=P
FROM OPENROWSET(BULK'p:\dotnet\MSSql\mssql-psyrnev\HW09_xml_json\StockItems.xml', SINGLE_BLOB) as T(P)

SELECT @x

-- XQuery
SELECT 
	i.Item.value('@Name', 'NVARCHAR(100)') AS StockItemName
	, i.Item.value('SupplierID[1]', 'int') AS SupplierID
	, i.Item.value('(Package/UnitPackageID)[1]', 'int') AS UnitPackageID
	, i.Item.value('(Package/OuterPackageID)[1]', 'int') AS OuterPackageID
	, i.Item.value('(Package/QuantityPerOuter)[1]', 'int') AS QuantityPerOuter
	, i.Item.value('(Package/TypicalWeightPerUnit)[1]', 'decimal(18,3)') AS TypicalWeightPerUnit
	, i.Item.value('LeadTimeDays[1]', 'int') AS LeadTimeDays
	, i.Item.value('IsChillerStock[1]', 'bit') AS IsChillerStock
	, i.Item.value('TaxRate[1]', 'decimal(18,3)') AS TaxRate
	, i.Item.value('UnitPrice[1]', 'decimal(18,3)') AS UnitPrice
FROM @x.nodes('/StockItems/Item') AS i(Item)

-- OPENXML
EXEC sp_xml_preparedocument @hdoc OUTPUT, @x

Select * INTO [Warehouse].[StockItemsXml]
FROM OPENXML (@hdoc,'/StockItems/Item') WITH (
    StockItemName NVARCHAR(100) '@Name'
    , SupplierID int 'SupplierID'
	, UnitPackageID int './Package/UnitPackageID'
	, OuterPackageID int './Package/OuterPackageID'
	, QuantityPerOuter int './Package/QuantityPerOuter'
	, TypicalWeightPerUnit decimal(18,3) './Package/TypicalWeightPerUnit'
	, LeadTimeDays int 'LeadTimeDays'
	, IsChillerStock bit 'IsChillerStock'
	, TaxRate decimal(18,3) 'TaxRate'
	, UnitPrice decimal(18,3) 'UnitPrice'
);

EXEC sp_xml_removedocument @hdoc

SELECT * FROM [Warehouse].[StockItemsXml] AS t

--Загузка в данных в таблицу Warehouse.StockItems:
MERGE [Warehouse].[StockItems] AS dst
USING [Warehouse].[StockItemsXml] AS src
    ON (dst.[StockItemName] = src.[StockItemName])
WHEN MATCHED 
    THEN UPDATE 
        SET 
			dst.SupplierID = src.SupplierID
			, dst.UnitPackageID = src.UnitPackageID
			, dst.OuterPackageID = src.OuterPackageID
			, dst.QuantityPerOuter = src.QuantityPerOuter
			, dst.TypicalWeightPerUnit = src.TypicalWeightPerUnit
			, dst.LeadTimeDays = src.LeadTimeDays
			, dst.IsChillerStock = src.IsChillerStock
			, dst.TaxRate = src.TaxRate
			, dst.UnitPrice = src.UnitPrice
WHEN NOT MATCHED 
    THEN INSERT (
		StockItemName
		, SupplierID
		, UnitPackageID
		, OuterPackageID
		, QuantityPerOuter
		, TypicalWeightPerUnit
		, LeadTimeDays
		, IsChillerStock
		, TaxRate
		, UnitPrice
		, LastEditedBy)
        VALUES (
			src.StockItemName
			, src.SupplierID
			, src.UnitPackageID
			, src.OuterPackageID
			, src.QuantityPerOuter
			, src.TypicalWeightPerUnit
			, src.LeadTimeDays
			, src.IsChillerStock
			, src.TaxRate
			, src.UnitPrice
			, 1)
OUTPUT deleted.*, $action, inserted.*;

/*
2. Выгрузить данные из таблицы StockItems в такой же xml-файл, как StockItems.xml
*/

SELECT DISTINCT t.[SupplierID] FROM [Warehouse].[StockItemsXml] AS t

SELECT 
	wsi.StockItemName AS [@Name]
	, wsi.[SupplierID] AS [SupplierID]
	, wsi.[UnitPackageID] AS [Package/UnitPackageID]
	, wsi.[OuterPackageID] AS [Package/OuterPackageID]
	, wsi.[QuantityPerOuter] AS [Package/QuantityPerOuter]
	, wsi.[TypicalWeightPerUnit] AS [Package/TypicalWeightPerUnit]
	, wsi.[LeadTimeDays] AS [LeadTimeDays]
	, wsi.[IsChillerStock] AS [IsChillerStock]
	, wsi.[TaxRate] AS [TaxRate]
	, wsi.[UnitPrice] AS [UnitPrice]
FROM [Warehouse].[StockItems] AS wsi
WHERE wsi.[SupplierID] IN (SELECT DISTINCT [SupplierID] FROM [Warehouse].[StockItemsXml])
FOR XML PATH('Item'), ROOT('StockItems');


/*
3. В таблице Warehouse.StockItems в колонке CustomFields есть данные в JSON.
Написать SELECT для вывода:
- StockItemID
- StockItemName
- CountryOfManufacture (из CustomFields)
- FirstTag (из поля CustomFields, первое значение из массива Tags)
*/

SELECT 
	wsi.StockItemID
	, wsi.StockItemName
	, JSON_VALUE(wsi.CustomFields, '$.CountryOfManufacture') AS CountryOfManufacture
	, JSON_VALUE(wsi.CustomFields, '$.Tags[0]') AS FirstTag
FROM [Warehouse].[StockItems] AS wsi

/*
4. Найти в StockItems строки, где есть тэг "Vintage".
Вывести: 
- StockItemID
- StockItemName
- (опционально) все теги (из CustomFields) через запятую в одном поле

Тэги искать в поле CustomFields, а не в Tags.
Запрос написать через функции работы с JSON.
Для поиска использовать равенство, использовать LIKE запрещено.

Должно быть в таком виде:
... where ... = 'Vintage'

Так принято не будет:
... where ... Tags like '%Vintage%'
... where ... CustomFields like '%Vintage%' 
*/

SELECT 
	subWsi.StockItemID
	, subWsi.StockItemName
	, REPLACE(subWsi.Tags, '"', '') AS Tags
FROM (
	SELECT
		wsi.StockItemID
		, wsi.StockItemName
		, LTRIM(
			RTRIM(
				JSON_QUERY(wsi.CustomFields, '$.Tags')
				, ']')
			, '[') AS Tags
	FROM [Warehouse].[StockItems] AS wsi
	) AS subWsi
CROSS APPLY (SELECT value as Tag FROM STRING_SPLIT(subWsi.Tags, ',')) AS caTag
WHERE caTag.Tag = '"Vintage"'
