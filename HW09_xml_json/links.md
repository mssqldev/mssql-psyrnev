
### Links:
- [habr: SQL Server JSON](https://habr.com/ru/articles/343062/)
- [sky.pro: XSLT для начинающих: основы, примеры и преобразование XML](https://sky.pro/wiki/javascript/xslt-dlya-nachinayushih-osnovy-primery-i-preobrazovanie-xml/)
- [XSLT](https://www.softwaretestinghelp.com/xslt-tutorial/)
- [YT: XSLT Tutorial](https://www.youtube.com/watch?v=HWwDMzZL9OY)
- [Пример экспорта/импорта в файл](https://docs.microsoft.com/en-us/sql/relational-databases/import-export/examples-of-bulk-import-and-export-of-xml-documents-sql-server)