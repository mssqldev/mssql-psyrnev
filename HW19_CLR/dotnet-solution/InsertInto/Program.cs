﻿// See https://aka.ms/new-console-template for more information

using System.Data;
using Microsoft.Data.SqlClient;
using System.IO;

var clubs = @"..\..\..\..\..\..\HW11.1_fullfil_db\clubs.csv";
var clubsBytes = File.ReadAllBytes(clubs);

string connectionString = "Data Source=DESKTOP-K2C08FA\\SQL2022; Integrated Security=true; Initial Catalog=DreamTeam; TrustServerCertificate=true";
using (SqlConnection connection = new SqlConnection(connectionString))
{
    connection.Open();
    string query = "INSERT INTO dt.[RawClubData] ([DocType], [Document]) VALUES (@DocType, @Document)";
    using (SqlCommand command = new SqlCommand(query, connection))
    {
        command.Parameters.AddWithValue("@DocType", 1);
        command.Parameters.Add(new SqlParameter("@Document", SqlDbType.VarBinary, 8000));
        command.Parameters["@Document"].Value = clubsBytes;
        //command.Parameters.Add(new VerticaParameter("@Document", VerticaType.Binary));
        command.ExecuteNonQuery();
    }
}