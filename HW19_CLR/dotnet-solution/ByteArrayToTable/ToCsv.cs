﻿using Microsoft.SqlServer.Server;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;

namespace ByteArrayToTable
{
    public static class ToCsv
    {
        private const char DEFAULT_SEPARATOR = ',';

        public static void MakeRow(
            object obj,
            out string item1,
            out string item2,
            out string item3)
        {
            var row = obj as string[];

            if (row == null)
            {
                item1 = null;
                item2 = null;
                item3 = null;
                return;
            }

            item1 = row[0];
            item2 = row[1];
            item3 = row[2];
        }


        [SqlFunction(FillRowMethodName=nameof(MakeRow))]
        public static IEnumerable Convert(SqlBinary data)
        {
            if (!IsValid(data))
            {
                yield break;
            }

            foreach (var row in GenericConvert(data.Value, DEFAULT_SEPARATOR))
            {
                yield return row;
            }
        }

        public static bool IsValid(SqlBinary data)
        {
            if (data == null)
            {
                return false;
            }

            if (data.IsNull)
            {
                return false;
            }

            return data.Length > 0;
        }

        public static IEnumerable<string[]> GenericConvert(byte[] data, params char[] separator)
        {
            if (data is null || data.Length == 0)
            {
                yield break;
            }

            using (var memoryStream = new MemoryStream(data))
            {
                using (var stream = new StreamReader(memoryStream))
                {
                    //skip the first line
                    stream.ReadLine();

                    while (!stream.EndOfStream)
                    {
                        var line = stream.ReadLine();

                        if (line == null)
                        {
                            continue;
                        }

                        var values = line.Split(separator);
                        yield return values;
                    }
                }
            }
        }
    }
}
