﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;

namespace ByteArrayToTable.Tests
{
    [TestClass]
    public class ToCsvTests
    {
        private static byte[] MakeByteFromCsv(string fileName)
        {
            return File.ReadAllBytes(fileName);
        }

        private static string[][] MakeStringFromCsv(string fileName)
        {
            return File.ReadAllLines(fileName)
                .Skip(1)
                .Select(l => l.Split(','))
                .ToArray();
        }


        [TestMethod]
        public void Convert_ValidSqlBinary_ReturnsExpectedCsv()
        {
            // Arrange
            var bytes = MakeByteFromCsv("Test1.csv");
            var data = new SqlBinary(bytes);
            var expected = MakeStringFromCsv("Test1.csv");

            // Act
            var result = ToCsv.Convert(data).Cast<string[]>().ToArray();

            // Assert
            Assert.AreEqual(expected.Length, result.Length);

            for (int i = 0; i < expected.Length; i++)
            {
                CollectionAssert.AreEqual(expected[i], result[i]);
            }
        }

        [TestMethod]
        public void Convert_InvalidSqlBinary_ReturnsEmpty()
        {
            // Arrange
            var data = SqlBinary.Null;

            // Act
            var result = ToCsv.Convert(data).Cast<string[]>().ToArray();

            // Assert
            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void Convert_ValidByteArray_ReturnsExpectedValues()
        {
            // Arrange
            var data = MakeByteFromCsv("Test1.csv");
            var expected = MakeStringFromCsv("Test1.csv");

            // Act
            var result = ToCsv.GenericConvert(data, ',').ToArray();

            // Assert
            Assert.AreEqual(expected.Length, result.Length);
            for (int i = 0; i < expected.Length; i++)
            {
                CollectionAssert.AreEqual(expected[i], result[i]);
            }
        }

        [TestMethod]
        public void Convert_EmptyByteArray_ReturnsEmpty()
        {
            // Arrange
            var data = Array.Empty<byte>();

            // Act
            var result = ToCsv.GenericConvert(data, ',').ToArray();

            // Assert
            Assert.AreEqual(0, result.Length);
        }
    }
}
