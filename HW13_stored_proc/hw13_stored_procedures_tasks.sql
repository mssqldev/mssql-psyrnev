/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "12 - Хранимые процедуры, функции, триггеры, курсоры".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

USE WideWorldImporters

/*
Во всех заданиях написать хранимую процедуру / функцию и продемонстрировать ее использование.
*/

/*
1) Написать функцию возвращающую Клиента с наибольшей суммой покупки.
*/

CREATE OR ALTER FUNCTION dbo.GetTopCustomer()
RETURNS int
AS 
BEGIN
    DECLARE @customerID int

    SELECT @customerID = subQ.[CustomerID]
    FROM 
    (
        SELECT TOP 1
            si.[CustomerID]
            , SUM(sil.[Quantity] * sil.[UnitPrice])  AS Cost
        FROM [Sales].[Invoices] AS si
            INNER JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
            GROUP BY si.[CustomerID], si.[InvoiceID]
        ORDER BY Cost DESC
    ) AS subQ

    RETURN @customerID
END;

SELECT dbo.GetTopCustomer()

/*
2) Написать хранимую процедуру с входящим параметром СustomerID, выводящую сумму покупки по этому клиенту.
Использовать таблицы :
Sales.Customers
Sales.Invoices
Sales.InvoiceLines
*/

-- Вариант 1

CREATE OR ALTER PROCEDURE dbo.GetCustomerPurchases(@customerID int)
AS 
BEGIN
    SELECT
        si.[InvoiceID]
        , SUM(sil.[Quantity] * sil.[UnitPrice])  AS Cost
    FROM
        [Sales].[Invoices] AS si
        INNER JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
    WHERE si.[CustomerID] = @customerID
    GROUP BY si.[InvoiceID]
END;

-- Вариант 2

CREATE OR ALTER PROCEDURE dbo.GetCustomerTotalSum(@customerID int)
AS 
BEGIN
    SELECT
        cs.[CustomerName]
        , SUM(sil.[Quantity] * sil.[UnitPrice])  AS Total
    FROM [Sales].[Customers] AS cs
        INNER JOIN [Sales].[Invoices] AS si ON cs.[CustomerID] = si.[CustomerID]
        INNER JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
    WHERE si.[CustomerID] = @customerID
    GROUP BY cs.[CustomerName]
END;

/*
3) Создать одинаковую функцию и хранимую процедуру, посмотреть в чем разница в производительности и почему.
*/
CREATE OR ALTER FUNCTION dbo.fGetCustomerPurchases(@customerID int)
RETURNS TABLE
AS 
RETURN (
    SELECT
        si.[InvoiceID]
        , SUM(sil.[Quantity] * sil.[UnitPrice])  AS Cost
    FROM
        [Sales].[Invoices] AS si
        INNER JOIN [Sales].[InvoiceLines] AS sil ON si.[InvoiceID] = sil.[InvoiceID]
    WHERE si.[CustomerID] = @customerID
    GROUP BY si.[InvoiceID]
)

SELECT * FROM dbo.fGetCustomerPurchases(834)
EXEC dbo.GetCustomerPurchases @customerID = 834

/*
4) Создайте табличную функцию покажите как ее можно вызвать для каждой строки result set'а без использования цикла. 
*/

SELECT TOP 10
    sc.[CustomerName]
    -- , cp.[InvoiceID]
    , cp.[Cost]
FROM [Sales].[Customers] AS sc
    CROSS APPLY dbo.fGetCustomerPurchases(sc.[CustomerID]) AS cp
-- WHERE sc.[CustomerID] = 834

/*
5) Опционально. Во всех процедурах укажите какой уровень изоляции транзакций вы бы использовали и почему. 
*/
